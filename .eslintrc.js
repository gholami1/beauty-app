module.exports = {
    "extends": [
    "react-app",
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:@typescript-eslint/recommended",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended"
    ],
    "plugins": [
    "react",
    "@typescript-eslint",
    "prettier"
    ],
    "env": {
    "browser": true,
    "jasmine": true,
    "jest": true
    },
    "rules": {
    "prettier/prettier": [
    "error", {
    "singleQuote": true,
    "tabWidth": 4
    }
    ],
    "@typescript-eslint/no-unused-vars": ["error", {
    "vars": "all",
    "args": "after-used",
    "ignoreRestSiblings": false
    }]
    },
    "settings": {
    "react": {
    "pragma": "React",
    "version": "detect"
    }
    },
    "parser": "@typescript-eslint/parser"
    }