import React, { Component } from 'react';
import Modal from 'react-modal';

class ReactModal extends Component {
    render() {
        const { isOpen, onRequestClose, children, style } = this.props;
        return (
            <div>
                <Modal
                    isOpen={isOpen}
                    onRequestClose={onRequestClose}
                    style={style}
                >
                    {children}
                    <button className="close" style={{position: 'absolute', top : '19px'}} onClick={onRequestClose} ><span className="text-3">X</span></button>
                </Modal>
            </div>
        );
    }
}

export default ReactModal;