import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { slider } from '../routes/path';

class SliderBackground extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      direction: null,
    };
  }

  handleSelect = (selectedIndex, e) => {
    this.setState({
      index: selectedIndex,
      direction: e.direction,
    });
  };
  render() {
    const { index, direction } = this.state;
    return (
      <>
        <Carousel
          direction={direction}
          activeIndex={index}
          onSelect={this.handleSelect}
          interval={100000}
          indicators={false}
        >
          {slider.map((slid, k) => {
            return (
              <Carousel.Item key={k}>
                <div className="item" val={k}>
                  <img className="img-slider" src={slid.img} alt={k} />
                </div>
              </Carousel.Item>
            );
          })}
        </Carousel>
      </>
    );
  }
}

export default SliderBackground;
