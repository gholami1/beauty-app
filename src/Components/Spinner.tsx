import React from 'react';
import { Backdrop , CircularProgress } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';


interface MyProps {
    open : boolean,
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    backdrop: {
        backgroundColor: '#fff',
        color: 'green',
    },
  }),
);

const Spinner : React.FC<MyProps> = ({ open }) => {
  const classes = useStyles();

  return (
    <div>
      <Backdrop className={classes.backdrop} open={open}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}
export default Spinner;