import React, { Component } from 'react';
import ReactModal from 'react-modal';
import Login from '../View/Login/Login';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { set_authenticated } from '../redux/actions/index';
import classnames from 'classnames';
import { requestFirebaseNotificationPermission , onMessageListener, getMessagingToken } from '../../src/helperFunctions/firebase-messaging';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      showBox: false,
    };
  }

  openLogin = () => {
    this.setState({
      showModal: true,
    });
  };

  handleCloseModal = (show) => {
    this.setState({
      showModal: show,
    });
  };

  exit = () => {
    this.props.set_authenticated(false);
    localStorage.clear();
    if (this.props.location.pathname !== '/') {
      this.props.history.push('/');
    }
  };

  handleOpenNotif = () => {
    this.setState({
      showBox : !this.state.showBox
    })
  }

  notificationPage = () => {
    this.props.history.push("/notif");
  }

  render() {
    const { isAuthenticated } = this.props;
    const user = JSON.parse(localStorage.getItem('user'));

    const showBox = classnames({
        'box-notif border-right-width-0 mt-2 ml-2': this.state.showBox,
        hide: !this.state.showBox,
    });


    return (
      <>
        <div className="main-menu">
          <nav className="primary-menu pb-3 navbar navbar-expand-lg">
            <div id="header-nav" className="collapse navbar-collapse col-10">
              <ul className="dropdown pr-0 pr-lg-5 navbar-nav">
                <li className="nav-item">
                  {isAuthenticated ? (
                    <div className="d-flex">
                      <a className="login px-3 py-1" onClick={this.exit}>
                        <i className="icon icon-16 icon-user ml-1"></i>
                        <span className="text-login">خروج</span>
                      </a>
                      {this.props.match.path === '/panel' ? (
                        <div className="nav-item mr-4 mt-1">
                          <span className="text-login">
                            {user.name} {user.last_name} خوش آمدید
                          </span>
                        </div>
                      ) : (
                        <div className="nav-item mr-4 mt-1">
                          <Link to="/panel">
                            <span className="text-login">پنل کاربری</span>
                          </Link>
                        </div>
                      )}
                    </div>
                  ) : (
                    <div>
                      <a
                        className="login px-3 py-1"
                        onClick={this.openLogin}
                      >
                        <i className="icon icon-16 icon-user ml-1 "></i>
                        <span className="text-login">ورود / عضویت</span>
                      </a>
                    </div>
                  )}
                </li>
              </ul>
            </div>
            <div className="header-column col-2" onClick={this.handleOpenNotif}  >
              <i className="icon icon-notification icon-24 cursor-pointer">
                <span className="icon-circle text-white text-2 text-center">3</span>
              </i>
            </div>
          </nav>
          
          <ReactModal
            className="ReactModal__Content"
            role="dialog"
            blockScroll={false}
            isOpen={this.state.showModal}
            onRequestClose={this.handleCloseModal}
            shouldCloseOnOverlayClick={true}
            style={{
              content: {
                overlfow: 'scroll',
                height: '100vh',
              },
              overlay: {
                backgroundColor: 'rgb(63,63,63,0.75)',
                zIndex: 10009,
              },
            }}
          >
            <Login onClose={this.handleCloseModal} />
          </ReactModal>
        </div>
        <div className="col-12 d-flex justify-content-end position-relative">
          <div className={showBox} value={this.state.showBox}>
            <div className="p-3 bg-white">
              <i className=""></i>
              <div className="text-center font-weight-bold">اعلانات</div>
              <i className=""></i>
            </div>
            <div className="p-2 d-flex border-right-aqua border-bottom-coal">
              <div className="col-9 pl-0 pr-1 text-right">
                <div className="cursor-pointer text-2 text-aqua" onClick={this.notificationPage}>کتی یانگ</div>
                <p className="text-2 mb-0 text-justify">
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از 
                </p>
              </div>
              <div className="text-1 text-coal px-0 col-3">
                5 دقیقه قبل
              </div>
            </div>
            <div className="p-2 d-flex border-bottom">
              <div className="col-9 pl-0 pr-1 text-right">
                <div className="cursor-pointer text-2 text-coal">کتی یانگ</div>
                <p className="text-2 mb-0 text-justify">
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از 
                </p>
              </div>
              <div className="text-1 text-coal px-0 col-3">
                5 دقیقه قبل
              </div>
            </div>
            <div className="p-2 d-flex border-bottom">
              <div className="col-9 pl-0 pr-1 text-right">
                <div className="text-2 text-coal">کتی یانگ</div>
                <p className="text-2 mb-0 text-justify">
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از 
                </p>
              </div>
              <div className="text-1 text-coal px-0 col-3">
                5 دقیقه قبل
              </div>
            </div>
            <div className="p-2 text-2 text-center cursor-pointer text-aqua bg-white">
            همه پیام های دریافتی را مشاهده کنید.
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
  };
};

export default connect(mapStateToProps, { set_authenticated })(withRouter(Menu));
