import React, { useState, useEffect } from 'react';
import { Link, withRouter } from "react-router-dom";


const CardMessage = ({ message, read, remove }) => {
    const [show_content, setshow_content] = useState(false);

    useEffect(() => {
    }, []);

    const toggle_collapse = () => {
        if (!message.read) {
            read(message.id);
        }
        show_content ? setshow_content(false) : setshow_content(true);
    }
    return (
        <div className="col-lg-12 col-md-12" onClick={() => toggle_collapse()}>
            <div className={`card-message ${message.read ? "message-read" : null}`}>
                <div className="d-flex flex-column">
                    <p >{message.title}</p>
                    {show_content ?
                        <p>{message.body}</p> : null
                    }
                    <div className="d-flex justify-content-between">
                        <p>12:30:00</p>
                        <p>99/5/5</p>
                    </div>
                </div>

                <div className={`d-flex 
                    justify-content-left ${show_content ?
                        "align-items-start" : "align-items-center"}
                     mx-2`}>
                    {show_content ?
                        <i className="icon icon-delete icon-24  d-flex 
                     justify-content-left align-items-start mx-2"
                            onClick={() => remove(message.id)}></i>
                        :
                        null
                    }
                    <i className="icon icon-email icon-24  d-flex 
                    justify-content-left align-items-start mx-2"></i>
                </div>
            </div>
        </div>
    );
};
export default withRouter(CardMessage);
