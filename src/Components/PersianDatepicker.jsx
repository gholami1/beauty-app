import React, { Component } from 'react';
import { DatePicker } from "react-persian-datepicker";

const styles = {
    calendarContainer: 'calendarContainer',
    dayPickerContainer: 'dayPickerContainer',
    monthsList: 'monthsList',
    daysOfWeek: 'daysOfWeek',
    dayWrapper: 'dayWrapper',
    selected: 'selected',
    heading: 'heading',
    prev: 'prev',
    next: 'next',
    title: 'title', 
}

export default class PersianDatePicker extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    handleChange(value) {
        this.props.onChange(value);
    }

    render(){
        const {className, disabled, placeholder} = this.props
        return(
            <DatePicker
                calendarStyles={styles}
                inputFormat="jYYYY/jM/jD"
                className={className}
                onChange={this.handleChange.bind(this)}
                value={this.props.value}
                disabled={disabled}
                placeholder={placeholder}
            />
        )
    }
}
