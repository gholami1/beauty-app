import React from 'react';

const Tab = (props) => {
    const { label, onClick } = props;

    const onClickTab = () => {
        onClick(label);
    }

    return (
        <>
             <li
                className="tab-list-item"
                onClick={onClickTab}
            >
                {label}
            </li>
        </>
    )
}

export default Tab;