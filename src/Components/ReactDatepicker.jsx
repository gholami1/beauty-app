import React, { Component } from "react";
import JalaliUtils from 'material-ui-pickers-jalali-utils';
import { DateTimePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import jMoment from 'moment-jalaali';

jMoment.loadPersian({ dialect: 'persian-modern', usePersianDigits: true });

class EditedJalaliUtils extends JalaliUtils {
  startOfMonth(date) {
      return date.clone().startOf('jMonth');
  }
}

class ReactDatepicker extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { onChange , value } = this.props;

    return (       
        <MuiPickersUtilsProvider utils={EditedJalaliUtils} locale="fa">
          <div className="picker">
            <DateTimePicker
                clearable
                okLabel="تأیید"
                cancelLabel="لغو"
                clearLabel="پاک کردن"
                labelFunc={date => (date ? date.format('jYYYY/jMM/jDD hh:mm A') : '')}
                value={value}
                onChange={onChange}
                animateYearScrolling={false}
                disablePast={true}
            />
          </div>
      </MuiPickersUtilsProvider>
    );
  }
}
export default ReactDatepicker;