import React from 'react';
import { Button, CircularProgress } from '@material-ui/core';
import { makeStyles , Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme : Theme) => ({
  buttonProgress: {
    color: theme.palette.primary.main,
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -8,
    marginLeft: -12
  }
}));

const ButtonLoading : React.FC = (props) => {
  const classes = useStyles();
  return (
      <></>
    // <Button {...rest} disabled={disabled === null ? loading : disabled}>
    //   {name}
    //   {loading && <CircularProgress size={20} className={classes.buttonProgress} />}
    // </Button>
  );
};

export default ButtonLoading;