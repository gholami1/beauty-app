import React, { Component } from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import { Icon } from "leaflet";

class MapBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      currentPos: null
    };
  }

  handleClick = e => {
    this.setState({
      lat: e.latlng.lat,
      lng: e.latlng.lng,
    });
    
    this.props.latlong(this.state.lat , this.state.lng);
  };

  render() {
    const { currentPos } = this.state;
    const { Latitude, Longitude, zoom } = this.props;
    return (
      <div id="login-signup">
        <div className="modal-dialog modal-width modal-dialog-centered">
          <div className="modal-content p-sm-3 bg-white">
            <div className="modal-body">
              <a
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => this.props.onClose()}
              >
                <i className="icon icon-close icon-20"></i>
              </a>

              <div className="leaflet-container">
                <Map
                  center={[Latitude, Longitude]}
                  zoom={zoom}
                  onClick={this.handleClick}
                >
                  <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                  />
                  <Marker position={currentPos ? currentPos : [Latitude, Longitude]}></Marker>
                </Map>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default MapBox;
