/* eslint-disable no-unused-expressions */
import axios from 'axios';
import Swal from 'sweetalert2';

let api_token;
const baseURL = `http://localhost:8000/api/`; //server

axios.defaults.baseURL = baseURL;
axios.defaults.timeout = 15000;
axios.defaults.headers.common.Accept = 'application/json';
axios.defaults.headers.common['Content-Type'] = 'application/json';
// axios.defaults.headers.common['Authorization'] = auth_token;
api_token = api_token
  ? api_token
  : JSON.parse(localStorage.getItem('api_token'));

axios.defaults.headers.common['api_token'] = api_token;

const headers = {
  // "X-Requested-With": "XMLHttpRequest",
  // Accept: "application/json"
  // "Content-Type": content_type || "application/json; charset=UTF8"
};

const xhr = axios.create({
  headers,
  baseURL,
  timeout: 10000,
  responseType: 'json',
});

export const setToken = token => api_token = token;

 function getToken() {
  if (api_token) {
    return api_token;
  } else {
      api_token =  localStorage.getItem('api_token');
      return api_token;
  }
}

function handleErr(err) {
  if (!err.response) {
    Swal.fire({
      title: '',
      text: 'خطایی رخ داده است لطفاً مجدداً تلاش کنید',
      icon: 'error',
      timer: 2500,
      confirmButtonText: 'بستن',
    });
  } else if (err.response.status === 422) {
    let error = err.response.data.errors;
    Swal.fire({
      title: '',
      text: error[Object.keys(error)[0]].join(),
      icon: 'error',
      timer: 2500,
      confirmButtonText: 'بستن',
    });
  } else if (err.response.status === 401) {
    api_token = null;
    Swal.fire({
      title: '',
      text: err.response.message
        ? err.response.message
        : 'شما باید مجددا وارد شوید',
      icon: 'error',
      timer: 2500,
      confirmButtonText: 'بستن',
    });
  } else if (err.response.status === 403) {
    api_token = null;
    Swal.fire({
      title: '',
      text: err.response.message ? err.response.message : 'اجازه دسترسی ندارید',
      icon: 'error',
      timer: 2500,
      confirmButtonText: 'بستن',
    });
  } else if (err.response.status === 429) {
    Swal.fire({
      title: '',
      text: err.response.message
        ? err.response.message
        : 'بعدا مجددا تلاش کنید',
      icon: 'error',
      timer: 2500,
      confirmButtonText: 'بستن',
    });
  }
}

export function get(url, params = null, token = false, showError = true) {
  token ? (params.api_token = getToken()) : null;
  return xhr.get(url, {params}).catch(err => {
    showError ? handleErr(err) : null;
    throw err.response;
  });
}

export async function post(url, data = null, token = false, showError = true) {
  token ? (data.api_token = await getToken()) : null;
  return xhr.post(url, data).catch(err => {
    showError ? handleErr(err) : null;
    throw err.response;
  });
}

export async function upload(url, data) {
  let api_token = await getToken();
  data.append('api_token', api_token);
  let options = {
    headers: {
      Accept: 'application/json',
      'Content-type': 'multipart/form-data',
    },
  };
  return axios.post(url, data, options).catch(err => {
    handleErr(err);
    throw err.response;
  });
}

export async function getCity() {
  if (await localStorage.getItem('cities')) {
    return JSON.parse(await localStorage.getItem('cities'));
  } else {
    let url = `/cities`;
    return axios
      .get(url)
      .then(res => {
        localStorage.setItem('cities', JSON.stringify(res));
        return res;
      })
      .catch(err => {
        console.log(err, 'err');
      });
  }
}

export async function getDetailsSalon(data) {
  try {
    const res = await axios.get(`/services?name_en=${data}`);
    return res;
  } catch (error) {
    console.log(error);
  }
}
