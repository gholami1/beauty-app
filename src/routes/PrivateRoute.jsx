import React from "react"
import { Redirect, Route } from "react-router-dom"

function PrivateRoute({  isAuthenticated , permission , ...props }) {
  return isAuthenticated && permission ? <Route {...props} /> : <Redirect to="/" />
}

export default PrivateRoute



