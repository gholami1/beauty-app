import Services from '../View/Salon/Services/Services';
import Salon from '../View/Salon/Salon';
import AwaitingFactors from '../View/Panel/AwaitingFactors';
import Messages from '../View/Panel/Messages';
import FutureFactors from '../View/Panel/FutureFactors';
import PastFactors from '../View/Panel/PastFactors';
import FavLocation from '../View/Panel/FavLocation';
import Payments from '../View/Panel/Payments';
import Credit from '../View/Panel/Credit';
import ChangePassword from '../View/Panel/ChangePassword';
import UserInfo from '../View/Panel/UserInfo';
import ServicesInfo from '../View/Panel/ServicesInfo';
import SalonInfo from '../View/Panel/SalonInfo';
import Home from '../View/Home/Home';
import SalonServices from '../View/Salon/SalonServices';
import Profile from '../View/Login/Profile';
import Notification from '../View/Panel/Messages';
import Img1 from '../assets/img/1.jpg';
import Img3 from '../assets/img/3.jpg';
import Img5 from '../assets/img/5.jpg';
import Img6 from '../assets/img/6.jpg';
import Img7 from '../assets/img/7.jpg';
import Img8 from '../assets/img/8.jpg';
import Img9 from '../assets/img/9.jpg';
import Img10 from '../assets/img/10.jpg';


const path = [
  {
    path : "/panel" ,
    component : Profile ,
    auth : true 
  },
  {path : "/notif" , component : Notification , auth : true },
  {path : "/" , component : Home, auth : false },
  {
    path: '/salon/listService/:stringSrch',
    titleName: 'لیست خدمات',
    component: Salon,
    auth : false,
  },
  {
    path: '/services/salonServices/',
    titleName: 'جزئیات خدمات',
    component: Services,
    param: 'param',
    breadCrumbUri: 'salonServices',
    auth : false,
  },
];

const panel = [
  {
    titleName: 'سفارش ها',
    id: 'orders',
    iconClass: 'icon-orderList',
    sidebar: true,
    auth : true,
    subs: [
      {
        path: '/panel/AwaitingFactors',
        titleName: 'سفارش های در انتظار',
        component: AwaitingFactors,
        breadCrumbUri: 'AwaitingFactors',
      },
      {
        path: '/panel/FutureFactors',
        titleName: 'سفارش های پیش رو',
        component: FutureFactors,
        breadCrumbUri: 'FutureFactors',
      },
      {
        path: '/panel/PastFactors',
        titleName: 'سفارش های گذشته',
        component: PastFactors,
        breadCrumbUri: 'PastFactors',
      },
    ],
  },
  {
    path: '/panel/servicesinfo',
    titleName: 'سرویس ها',
    component: ServicesInfo,
    iconClass: 'icon-userInfo',
    sidebar: true,
    breadCrumbUri: 'servicesinfo',
    auth : true,
  },
  // {
  //   path: "/panel/servicesinfo/addservice",
  //   titleName: "ایجاد سرویس",
  //   component:  AddService,
  //   breadCrumbUri: "addservice",
  // },
  // {
  //   path : "/panel/servicesinfo/editservice",
  //   titleName : "ویرایش سرویس",
  //   component : AddService,
  //   breadCrumbUri: "editservice",
  // },
  {
    path: '/panel/userinfo',
    titleName: 'اطلاعات کاربری',
    component: UserInfo,
    iconClass: 'icon-userInfo',
    sidebar: true,
    breadCrumbUri: 'userinfo',
    auth : true,
  },
  {
    path: '/panel/saloninfo',
    titleName: 'ویرایش اطلاعات آرایشگاه',
    component: SalonInfo,
    iconClass: 'icon-userInfo',
    sidebar: true,
    breadCrumbUri: 'saloninfo',
    auth : true,
  },
  {
    path: '/panel/favLocation',
    titleName: 'محل های محبوب من',
    component: FavLocation,
    breadCrumbUri: 'Profile',
    iconClass: 'icon-pin',
    sidebar: true,
    breadCrumbUri: 'favLocation',
    auth : true
  },
  {
    path: '/panel/payments',
    titleName: 'پرداختی های من',
    component: Payments,
    breadCrumbUri: 'Profile',
    iconClass: 'icon-creditCard',
    sidebar: true,
    breadCrumbUri: 'payments',
    auth : true
  },
  {
    path: '/panel/IncreaseCredit',
    titleName: 'افزایش اعتبار',
    component: Credit,
    breadCrumbUri: 'Profile',
    iconClass: 'icon-wallet',
    sidebar: true,
    breadCrumbUri: 'IncreaseCredit',
    auth : true
  },
  {
    path: '/panel/changePassword',
    titleName: 'تغییر رمز عبور',
    component: ChangePassword,
    iconClass: 'icon-password',
    sidebar: true,
    breadCrumbUri: 'changePassword',
    auth : true,
  },
];

const slider = [
  {
    img: Img1,
  },
  {
    img: Img3,
  },
  {
    img: Img5,
  },
  {
    img: Img6,
  },
  {
    img: Img7,
  },
  {
    img: Img8,
  },
  {
    img: Img9,
  },
  {
    img: Img10,
  },
];
export { slider, panel, path };
