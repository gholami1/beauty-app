String.prototype.toPersian = function() {
    const id = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    return this.replace(/[0-9]/g, function(w) {
        return id[+w];
    });
};

String.prototype.toEnglish = function() {
    const find = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    var replace = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    var replaceString = this;
    var regex;
    for(var i = 0; i < find.length; i++){
        regex = new RegExp(find[i], "g");
        replaceString = replaceString.replace(regex, replace[i]);
    }
    return replaceString;
};




