import * as firebase from "firebase/app";
import "firebase/messaging";
import {
  post
} from "../services/api";

const initializedFirebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyBJ9jXKQCw9B06jdy9g7E3b-xCEQuw8Y-0",
  authDomain: "makeupi-72f99.firebaseapp.com",
  databaseURL: "https://makeupi-72f99.firebaseio.com",
  projectId: "makeupi-72f99",
  storageBucket: "makeupi-72f99.appspot.com",
  messagingSenderId: "464422658172",
  appId: "1:464422658172:web:81d48e13d78c3940d369f9",
  measurementId: "G-CYXP90FLPS"
});
const messaging = initializedFirebaseApp.messaging();
const vapidKey = "BMN3NgrNR16HphHkVd0fhHiKoGKxsnV2RGz7Umi_PR_UZjGm7I2fzSGMlWZAUs4PduCUZo2CeultLlCLii3Zf8A";
messaging.usePublicVapidKey(vapidKey);



export const getMessagingToken =
  () => messaging.getToken().then((fcm_token) => {
    if (fcm_token) {
      console.log(fcm_token, 'fcm_token');
      post('/user/fcm_token', {
        fcm_token
      }, true, false);
      // saveToken(token);
      // } else {
      // Show permission request.
      // console.log('No Instance ID token available. Request permission to generate one.');
    }
  }).catch((err) => {
    console.log('An error occurred while retrieving token. ');
    //   showToken('Error retrieving Instance ID token. ', err);
    //   setTokenSentToServer(false);
  });

export const onMessageListener = (callback) => {
  return messaging.onMessage(() => callback);
  // console.log(onMessage, onError, onCompleted,'messageeeeee');
}

export const onTokenRefresh = () => {
  messaging.onTokenRefresh(() => {
    messaging.getToken().then((fcm_token) => {
      console.log(fcm_token, 'fcm_token');
      post('/user/fcm_token', {
        fcm_token
      }, true, false);
    }).catch((err) => {
      console.log('Unable to retrieve refreshed token ', err);
    });
  });
}
