import React , { Component } from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../src/assets/css/customStyle.css';
import '../src/assets/css/stylesheet.css';
import '../src/assets/css/fonts.css';
import '../src/assets/css/icon.css';
import { store, persistor } from './redux/store'; 
import { PersistGate } from 'redux-persist/integration/react';
import App from './App';
import * as serviceWorker from './serviceWorker';

export default class Root extends Component {
    constructor(props){
        super(props);
        this.state = {}
    }

    render() {
        return (
            <Provider store={ store }>
                <PersistGate persistor={persistor}>
                    <App />
                </PersistGate>
            </Provider>
        )
    }
}

ReactDOM.render(<Root />, document.getElementById('root'));
serviceWorker.register();