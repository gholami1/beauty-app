import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

interface searchData {
    sex: any,
    city: string,
    Neighborhood: string,
    services: string,
    salon: string,
    [key: string]: string;
};

const Search : React.FC = (props) => {
    const [searchDa , setSearchDa] = useState({
        city : "",
        neighborhood : "",
        services : "",
        salon : ""
    });
    const [value , setValue] = useState("1");

    let dataserch : string;
    let history = useHistory();

    const selectHandle = (e : any) => {
        const target = e.target;
        const val = target.type === "checkbox" ? target.checked : target.value;
        setValue(val);
    }

    const handleChange = (e : any) => {
        setSearchDa({...searchDa,[e.target.name] :e.target.value});
    }

    const search = () => {
        
        let search : Partial<searchData> = {
            "sex": value,
        }
        if(searchDa.city) {
            search = {
                "city": searchDa.city,
                ...search,
            }
        }
        if(searchDa.neighborhood) {
            search = {
                "Neighborhood": searchDa.neighborhood,
                ...search,
            }
        }
        if(searchDa.services) {
            search = {
                "services": searchDa.services,
                ...search,
            }
        }
        if(searchDa.salon) {
            search = {
                "salon": searchDa.salon,
                ...search,
            }
        }
        
        let stringSrch : string = '';
        for (var key in search) {
            stringSrch += `${search[key]}&`
        }
        
        dataserch = stringSrch.substring(0, stringSrch.length - 1);
        history.push(`/salon/listService/${dataserch}` , { search: search , loaderSpin: true });
    }

    return (
        <div className="search col-lg-8 col-md-8 col-sm-12 col-10 mx-auto search-wrapper py-3">
            <div className="row pr-4 pr-sm-5 pb-2">
                <div className="custom-control custom-radio">
                    <input id="female" name="flight-class" className="flight-class custom-control-input" onChange={selectHandle} required type="radio" value={"1"} defaultChecked />
                    <label className="custom-control-label text-light" htmlFor="female">زن</label>
                </div>
                <div className="custom-control custom-radio mr-1">
                    <input id="men" name="flight-class" className="flight-class custom-control-input" onChange={selectHandle} required type="radio" value={"0"} />
                    <label className="custom-control-label text-light" htmlFor="men">مرد </label>
                </div>
            </div>
            <div className="row pr-sm-4 no-gutters justify-content-around search-input-2">
                <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
                    <input id="city" type="text" className="form-control input-search" required placeholder="شهر" name="city" onChange={handleChange} value={searchDa.city} />
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
                    <input id="Neighborhood" type="text" className="form-control input-search" required placeholder="محله" name="neighborhood" onChange={handleChange} value={searchDa.neighborhood} />
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
                    <input id="Services" type="text" className="form-control input-search" required placeholder="خدمات" name="services" onChange={handleChange} value={searchDa.services} />
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-5 px-lg-2 form-group">
                    <input id="salon" type="text" className="form-control input-search" required placeholder="سالن" name="salon" onChange={handleChange} value={searchDa.salon} />
                </div>
                <div className="search-submit col-lg-3 col-md-3 col-sm-3 col-12 justify-content-center form-group">
                    <button className="btn searchBtn w-100" type="submit" onClick={search}>جستجو</button>
                </div>
            </div>
        </div>
    );
}
export default Search;