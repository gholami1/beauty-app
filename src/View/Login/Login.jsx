import React, { Component } from 'react';
import { get, post, auth, doRegister, request_verification, login, register, resetPass, ApiPasswordChange } from '../../services/api';
import Loader from 'react-loader-spinner';
import Swal from 'sweetalert2';
import { connect } from "react-redux";
import { set_authenticated } from "../../redux/actions/index";


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: "",
            step: 1,
            reset_password: false,
            code: "",
            spiner: false,
            resend: true,
            minutes: 0,
            seconds: 0,
        }
    }

    handlecheck = (e) => {
        e.preventDefault();
        const target = e.target;
        this.setState({
            [target.name]: target.value,
        })
    }

    authentication = async () => {
      
        const mobile = this.state.phone;
        if (mobile) {
            this.setState({
                spiner: true,
            })
            try {
                let res = await get('/check_mobile', {mobile}, false);
                    this.setState({
                        result: res.data.data.message,
                    }, () => { this.handleStep() });
            } catch(err) {
                    console.log('err', err);
                };
        }
    }

    handlePassRecovery = () => {
        this.setState({
            step: 5,
        })
    }

    timer = () => {
        console.log(this.state.resend, 'resend');
        this.setState({
            minutes: 1,
            seconds: 0,
        })
        this.countDown = setInterval(() => {
            const { minutes, seconds } = this.state;
            if (seconds > 0) {
                this.setState({
                    seconds: seconds - 1
                })
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(this.countDown);
                    this.setState({
                        resend: false,
                    })
                }
                else {
                    this.setState({
                        minutes: minutes - 1,
                        seconds: 59,
                    })
                }
            }
        }, 1000)
    }

    handleStep = () => {
        if (this.state.result !== "exist") {
            this.setState({
                step: 2,
                spiner: false,
            }, this.timer())
        } else if (this.state.result === "exist") {
            this.setState({
                step: 3,
                spiner: false,
                // resend: true,
            });
        }
    }

    switchRender = (step) => {
        switch (step) {
            case 1:
                return this.checkMobile();
            case 2:
                return this.checkCode();
            case 3:
                return this.login();
            case 4:
                return this.register();
            case 5:
                return this.passRecovery();
            case 6:
                return this.passwordChange();
            default:
                break;
        }
    }

    checkMobile = () => {
        return (
            <div className="row">
                <div className="col-12 mt-5 text-center">
                    لطفا برای <b>ورود</b> یا <b>ثبت نام</b> شماره همراه خود را وارد کنید.
                </div>
                <div className="form-group text-right mt-5 col-lg-12 col-12">
                    <input type="text" name="phone" className="form-control form-ctrl mt-2 text-right" autoComplete="off" value={this.state.phone} onChange={(e) => this.handlecheck(e)} placeholder="*********09" />
                </div>
                <div className="form-group mt-5 col-lg-12 col-12">
                    <button
                        className="btn btn-search col-11 col-lg-12"
                        onClick={this.authentication}
                        >
                        {this.state.spiner ?
                            <div>
                                <Loader type="ThreeDots" color="#5edfff" height={30} width={30} />
                            </div>
                            : 'ادامه'}
                    </button>
                </div>
            </div>
        )
    }


    checkCode = () => {
        return (
            <div className="tab-content pt-4">
                <div className="tab-pane fade show active" id="signup" role="tabpanel" aria-labelledby="signup-tab">
                    {!this.state.reset_password ? <p className="text-center mt-4 mb-5 pb-1">لطفاً برای تکمیل ثبت نام کد ارسال شده را وارد کنید</p> :
                        <p className="text-center my-4 pb-1">لطفاً برای بازیابی رمز عبور کد ارسال شده را وارد کنید</p>}
                    <div className="form-group text-right">
                        {this.state.phone ? this.state.phone : ''}
                    </div>
                    <div className="form-group">
                        <input type="text" class="form-control form-ctrl" name="code" data-bv-field="number" autoComplete="off" value={this.state.code} required placeholder="کد ارسالی را وارد کنید" onChange={e => this.handlecheck(e)} />
                    </div>
                    <button className="btn btn-search btn-block mt-5" type="submit" onClick={() => this.submitCheckCode()}>
                        {
                            this.state.spiner ?
                                <div>
                                    <Loader type="ThreeDots" color="#5edfff" height={30} width={30} />
                                </div>
                                : 'تایید'
                        }
                    </button>

                    <div className="text-left resend">
                        {this.state.resend ?
                            <a className="send-code">
                                <span className="" > {this.state.seconds < 10 ? `0${this.state.seconds}` : this.state.seconds} : {this.state.minutes} </span>
                            </a>
                            :
                            <div className="send-code" onClick={this.handleResendCode}>
                                درخواست مجدد کد
                            </div>
                        }
                    </div>
                </div>
            </div>
        )
    }

    submitCheckCode = async () => {

        if (this.state.code) {
            this.setState({
                spiner: true,
            })
            const code = {
                "mobile": this.state.phone,
                "code": this.state.code,
            }
            try {
                let res =await get('/verification' , { code } , false);
                    if (res.data.status === "success") {
                        this.state.reset_password ?
                            this.setState({
                                step: 6,
                                spiner: false,
                                reset_password: false,
                            }) : this.setState({
                                step: 4,
                                spiner: false,
                            });
                    } else {
                        console.log('err');
                    }
            } catch (err) {
                console.log('err', err);
            };
        }
    }


    register = () => {
        return (
            <div className="tab-content pt-4">
                <div className="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">

                    <div className="form-group">
                        <input type="text" class="form-control form-ctrl" name="name" id="name" required autoComplete="off" placeholder="نام کاربری" value={this.state.name} onChange={(e) => this.handlecheck(e)} />
                    </div>
                    <div className="form-group">
                        <input type="text" class="form-control form-ctrl" name="lastName" id="lastName" autoComplete="off" required placeholder="نام خانوادگی" value={this.state.lastName} onChange={(e) => this.handlecheck(e)} />
                    </div>
                    <div className="form-group">
                        <input type="password" class="form-control form-ctrl" name="password" id="password" autoComplete="off" required placeholder="رمز عبور" value={this.state.password} onChange={(e) => this.handlecheck(e)} />
                    </div>
                    <div className="form-group">
                        <input type="password" class="form-control form-ctrl" name="rePassword" id="rePassword" autoComplete="off" required placeholder="تکرار رمز عبور" value={this.state.rePassword} onChange={(e) => this.handlecheck(e)} />
                    </div>
                    <button className="btn btn-search btn-block p-1 mt-5" type="submit" onClick={(e) => this.doRegister(e)}>
                        {this.state.spiner ?
                            <div>
                                <Loader type="ThreeDots" color="#5edfff" height={30} width={30} />
                            </div>
                            :
                            'ورود'}
                    </button>
                </div>
            </div>
        )
    }

    passRecovery = () => {
        return (
            <div className="tab-content pt-4">
                <div className="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                    <div className="col-12 text-center mb-5">رمز عبور خود را فراموش کرده‌اید؟</div>
                    <div className="border-bottom text-right">{this.state.phone}</div>
                    <br /><br />
                    <button className="btn btn-search btn-block p-1 mt-5" type="submit" onClick={() => this.PassRec()}>تایید</button>
                </div>
            </div>
        )
    }

    PassRec =async () => {
        try {
            const mobile = this.state.phone;
            let res = await get('/password/reset', { mobile } , false)
                const { message } = res.data.data;
                this.setState({
                    reset_password: true,
                    step: 2,
                    // spiner: false,
                }, this.timer());
        } catch (err) {
                console.log('err', err);
            };

    }


    doRegister = async (e) => {
        try {
            const detailsUser = {
                "name": this.state.name,
                "last_name": this.state.lastName,
                "password": this.state.password,
                "password_confirmation": this.state.rePassword,
                "mobile": this.state.phone,
                "code": this.state.code,
            }
            this.setState({
                spiner: true,
            })
            let res =await post('/register', detailsUser, false);
                this.setState({
                    spiner: false,
                });
                if (res.status === 201) {
                    let user = res.data.data;
                    localStorage.setItem('user', JSON.stringify(this.state.user));
                    localStorage.setItem('api_token', JSON.stringify(this.state.user.api_token));
                    this.props.set_authenticated(true);
                } else {
                    console.log('err');
                }
        } catch(err) {
                console.log('err', err);
            };
    }


    passwordChange = () => {
        return (
            <div className="tab-content pt-4">
                <div className="text-center mb-3">تغییر رمز عبور</div>
                <div className="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                    <div className="form-group">
                        <input type="password" class="form-control form-ctrl" name="password" id="password" autoComplete="off" required value={this.state.password} onChange={(e) => this.handlecheck(e)} />
                    </div>
                    <div className="form-group">
                        <input type="password" class="form-control form-ctrl" name="rePassword" id="rePassword" autoComplete="off" required value={this.state.rePassword} onChange={(e) => this.handlecheck(e)} />
                    </div>
                    {
                        !this.state.resResetPass ?
                            <button className="btn btn-search btn-block p-1 mt-5" type="submit" onClick={this.confirmPass}>
                                تایید
                            </button>
                            :
                            <button className="btn btn-search btn-block p-1 mt-5" type="submit" onClick={() => this.props.onClose()}>
                                بستن
                            </button>
                    }
                </div>
            </div>
        )
    }

    confirmPass =async () => {
        try {
            const data = {
                "mobile": this.state.phone,
                "code": this.state.code,
                "password": this.state.password,
                "password_confirmation": this.state.rePassword,
            }
    
            let res =await post('/password/change', data, false);
                this.setState({
                    resResetPass: true,
                    // result: res.data.data.message,
                });
                Swal.fire({
                    title: '',
                    text: 'رمز شما با موفقیت تغییر پیدا کرد',
                    icon: 'success',
                    timer: 2500,
                    confirmButtonText: 'تایید'
                  })
        } catch (err) {
                console.log('err', err);
        };
    }


    handleResendCode = async () => {
        try {
            this.setState({
                resend: true
            })
            const mobile = this.state.phone;
            let res =await get('/check_mobile', { mobile } , false)
                    this.setState({
                        result: res.data.data.message,
                    });
        }
        catch(err) {
            console.log('err', err);
        };
        // if (!this.state.result === "send_sms") {
        this.timer();
        // }
    }


    login = () => {
        return (
            <div className="tab-content pt-4">
                <div className="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                    <div className="col-12 text-center mb-5">رمز عبور خود را وارد کنید</div>
                    <div className="text-right border-bottom">
                        <span className="text-right">{this.state.phone}</span>
                    </div>
                    <br />
                    <div className=""></div>
                    <div className="form-group mb-1 mt-5">
                        <input type="password" className="form-ctrl form-control" name="password" id="loginPassword" required placeholder="رمز عبور" onChange={(e) => this.handlecheck(e)} value={this.state.password} />
                    </div>
                    <div className="text-left pass-recovery mb-5" onClick={this.handlePassRecovery}>رمز عبور خود را فراموش کرده اید ؟</div>
                    {this.state.login ?
                        this.props.onClose(false)
                        : 
                        <button className="btn btn-primary btn-block p-1" type="submit" onClick={(e) => this.doLogin(e)}>ورود</button>
                    }
                </div>
            </div>
        )
    }

    doLogin = async (e) => {
        try {
            e.preventDefault();
            const userPass = {
                "mobile": this.state.phone,
                "password": this.state.password,
            }
            let res = await post('/login', userPass , false);
                localStorage.setItem('user' ,JSON.stringify(res.data.data));
                localStorage.setItem('api_token', JSON.stringify(res.data.data.api_token));
                this.props.set_authenticated(true);
                this.setState({
                    login: true,
                });
                Swal.fire({
                    title: '',
                    text: 'خوش آمدید',
                    icon: 'success',
                    timer: 3000,
                    confirmButtonText: 'تایید'
                  });
        } catch(err) {
                console.log(err, 'خطایی رخ داده است.')
        };
           
    }

    render() {
        const { step } = this.state;
        const { onClose } = this.props;
        return (
            <div id="login-signup">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content p-sm-3 bg-white">
                        <div className="modal-body">
                            <a data-dismiss="modal" aria-label="Close" onClick={() => onClose()}>
                                <i className="icon icon-close icon-20"></i>
                            </a>
                            {this.switchRender(step)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
    }
}
export default connect(mapStateToProps, { set_authenticated })(Login);