import React, { Component } from 'react';
import { createBrowserHistory } from 'history';
import { panel } from '../../routes/path';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import classNames from 'classnames';
import Menu from '../../Components/Menu';
import Panel from './Panel';
import BreadCrumb from '../../Components/BreadCrumb';
import Messages from '../Panel/Messages';
import AwaitingFactors from '../Panel/AwaitingFactors';
import FutureFactors from '../Panel/FutureFactors';
import PastFactors from '../Panel/PastFactors';
import Payments from '../Panel/Payments';
import ServicesInfo from '../Panel/ServicesInfo';
import UserInfo from '../Panel/UserInfo';
import SalonInfo from '../Panel/SalonInfo';
import Credit from '../Panel/Credit';
import ChangePassword from '../Panel/ChangePassword';
import FavLocation from '../Panel/FavLocation';

// const hist = createBrowserHistory();
// const switchRoutes = (
//   <Switch history={hist}>
//     {panelRoutes.map((prop, key) => {
//       if (prop.redirect) {
//         return <Redirect from={prop.path} to={prop.to} key={key} />;
//       }
//       if (prop.subs) {
//         return prop.subs.map((sub, k) => {
//           return <Route path={sub.path} component={sub.component} key={k} />;
//         });
//       }
//       if (prop.param) {
//         return (
//           <Route
//             path={`${prop.path}:${prop.param}`}
//             component={prop.component}
//             key={key}
//           />
//         );
//       }
      // if (prop.redirect) {
      // return <Route path={`${prop.path}:${prop.redirect}`} component={prop.component} key={key} />;
      // }
//       else
//         return <Route path={prop.path} component={prop.component} key={key} />;
//     })}
//   </Switch>
// );

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openPanel: false,
    };
  }

  collapseSub = (id) => {
    this.setState({
      openPanel: !this.state.openPanel,
    });
    // if (window.sessionStorage.collapseId === id) {
    //   window.sessionStorage.collapseId = null;
    //   this.setState({
    //     collapseId: null,
    //     openIcon: false,
    //   });
    // } else {
    //   window.sessionStorage.collapseId = id;
    //   this.setState({
    //     collapseId: id,
    //     openIcon: true,
    //   });
    // }
  };

  handleTab = (tab) => {
    this.setState({
      tab,
    });
  };

  showTab = () => {
    switch (this.state.tab) {
      case 'Messages':
        return <Messages />;
      case 'AwaitingFactors':
        return <AwaitingFactors />;
      case 'FutureFactors':
        return <FutureFactors />;
      case 'PastFactors':
        return <PastFactors />;
      case 'payments':
        return <Payments />;
      case 'servicesinfo':
        return <ServicesInfo />;
      case 'userinfo':
        return <UserInfo />;
      case 'saloninfo':
        return <SalonInfo />;
      case 'favLocation':
        return <FavLocation />;
      case 'IncreaseCredit':
        return <Credit />;
      default:
        return <ChangePassword />;
    }
  };

  render() {
    const open = classNames({
      'p-3 bordered-bottom': true,
      'd-block': this.state.openPanel,
      'd-none': !this.state.openPanel,
    });

    const borderBottom = classNames({
      'bordered-bottom pb-3': !this.state.openPanel,
      'bordered-none': this.state.openPanel,
    });

    return (
      <>
        {/* <Menu routes={panelRoutes} location={this.props.location} /> */}
        <BreadCrumb />
        <section className="container">
          <div className="d-flex">
            <div className="col-12 col-md-4 col-sm-5 col-lg-3 ml-3 my-5 border-box text-right">
              {panel
                ? panel.map((tab) => {
                  console.log(tab, 'ta11111b');
                  if (tab.id && tab.subs) {
                    return (
                      <>
                        <div
                          className={`text-right py-2 ${borderBottom}`}
                          onClick={() => this.collapseSub(tab.id)}
                        >
                          <i
                            className={`icon ${tab.iconClass} icon-20 ml-1`}
                          ></i>
                          <div className="" onClick={this.handleCom}>{tab.titleName}</div>
                        </div>
                        <div className={open}>
                          {tab.subs.map((t, k) => {
                            return (
                              <div key={k} onClick={this.handle}>
                                {t.titleName}
                              </div>
                            );
                          })}
                        </div>
                      </>
                    );
                  }
                  console.log(tab, 'tab');
                  return <Panel getTab={this.handleTab} tabList={tab} />;
                })
                : ''}
              <Panel getTab={this.handleTab} />
            </div>
            <div className="col-12 col-md-8 col-sm-7 col-lg-9 pt-1">
              {/* {switchRoutes} */}
              {this.showTab()}
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default withRouter(Profile);
