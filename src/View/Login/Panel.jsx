import React, { Component } from 'react';
import { Switch, Route, withRouter, Link } from 'react-router-dom';
import classNames from 'classnames';

class Panel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sideBarBack: false,
      collapseId: null,
      drawerOpen: false,
      openIcon: false,
      openPanel: false,
    };
  }

  componentWillMount() {
    if (window.sessionStorage.collapseId) {
      this.setState({
        collapseId: window.sessionStorage.collapseId,
      });
    }
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? true : false;
  }

  sideBarBack() {
    const { handleDrawerBack } = this.props;
    handleDrawerBack();
  }

  // collapseSub(id) {
  //   if (window.sessionStorage.collapseId === id) {
  //     window.sessionStorage.collapseId = null;
  //     this.setState({
  //       collapseId: null,
  //       openIcon: false,
  //     });
  //   } else {
  //     window.sessionStorage.collapseId = id;
  //     this.setState({
  //       collapseId: id,
  //       openIcon: true,
  //     });
  //   }
  // }

  handleDrawer() {
    const { handleDrawerToggle } = this.props;
    this.state.drawerOpen = !this.state.drawerOpen;
    handleDrawerToggle();
  }

  handleClick = (param) => {
    this.props.getTab(param);
  };

  render() {
    const { drawerMobileOpen, routes, tabList } = this.props;
    const { openIcon } = this.state;
    const links = [];

    
    const handleIcon = classNames({
      'icon icon-12 mt-2': true,
      'icon-arrowUp': openIcon,
      'icon-arrowDown': !openIcon,
    });

    const open = classNames({
      'p-3 bordered-bottom': true,
      'd-block': this.state.openPanel,
      'd-none': !this.state.openPanel,
    });

    const borderBottom = classNames({
      'bordered-bottom py-3': !this.state.openPanel,
      'bordered-none pt-3': this.state.openPanel,
    });

    // routes.map((prop, key) => {
    //   if (prop.redirect) return;
    //   if (prop.param) return;
    // if (
    //   prop.userTypes &&
    //   !prop.userTypes.includes(+localStorage.userType) &&
    //   localStorage.userType != 0
    // )
    //   return;
    //   if (!prop.titleName) return;
    //   if (prop.sidebar) {
    //     if (prop.subs) {
    //       const subs = [];
    //       prop.subs.map((sub, k) => {
    //         subs.push(
    //           <li className="nav-item my-1" key={k}>
    //             <Link
    //               to={sub.path}
    //               className={
    //                 'nav-link font-size-13 mr-5' +
    //                 (this.props.location &&
    //                 this.props.location.pathname === sub.path
    //                   ? ' active'
    //                   : '')
    //               }
    //             >
    //               {sub.titleName}
    //             </Link>
    //           </li>
    //         );
    //       });
    //       links.push(
    //         <li className="nav-item" key={key}>
    //           <div
    //             className="col-lg-12 pr-0"
    //             onClick={() => this.collapseSub(prop.id)}
    //           >
    //             <div className="d-flex">
    //               <div className="col-lg-10">
    //                 <a
    //                   name={prop.id}
    //                   className={`nav-link text-right py-2 px-0 cursor `}
    //                 >
    //                   <i className={'icon icon-20 ml-2 ' + prop.iconClass}></i>
    //                   <span className="text-right text-dark small">
    //                     {prop.titleName}
    //                   </span>
    //                 </a>
    //               </div>
    //               <div className="col-lg-1">
    //                 <i className={handleIcon}></i>
    //               </div>
    //             </div>
    //           </div>
    //           <div
    //             className={
    //               'collapse text-right ' +
    //               (this.state.collapseId === prop.id ? 'show ' : '')
    //             }
    //             id={prop.id}
    //           >
    //             <ul className="nav flex-column">{subs}</ul>
    //           </div>
    //         </li>
    //       );
    //     } else {
    //       links.push(
    //         <li className="nav-item" key={key}>
    //           <Link
    //             to={prop.sidebar ? prop.path : ''}
    //             className={
    //               'nav-link' +
    //               (this.props.location &&
    //               this.props.location.pathname === prop.path
    //                 ? ' act '
    //                 : '')
    //             }
    //           >
    //             <i className={'icon icon-20 ml-2 ' + prop.iconClass}></i>
    //             <span className="small text-dark">{prop.titleName}</span>
    //           </Link>
    //         </li>
    //       );
    //     }
    //   }
    // });

    return (
      <>
        {/* <div className="side-bar-wrapper col-auto h-100">
          <div className="side-bar mt-3 h-100"> */}
        {/* <div className="header justify-content-between d-flex"> */}
        {/* <div className="mt-3 mr-3"> */}
        {/* <button type="button" onClick={() => this.handleDrawer()} className="btn btn-link btn-compact-side-bar d-none d-lg-inline-block">
                  <i className="icon icon-24 icon-menu"></i>
                </button>
                <button type="button" onClick={() => this.sideBarBack()} className="btn btn-link btn-show-side-bar d-inline-block d-lg-none">
                  <i className="icon icon-24 icon-arrow-right-muted"></i>
                </button> */}
        {/* </div> */}
        {/* </div> */}
        {/* <div className="menu">
              <div className="d-flex mb-4 pr-3 side-bar-menu-profile">
                <div> */}
        {/* <img src={(localStorage.avatar && localStorage.avatar != "null" && localStorage.avatar.length > 0) ? baseUrl + localStorage.avatar : "/img/pic.png"} alt="پروفایل" className="side-bar-profile-pic" /> */}
        {/* </div>
                <div className="mt-0 pr-3 side-bar-menu-welcome">
                  <h6 className="small">خوش آمدید</h6>
                  <h6 className="small text-right">
                
                  </h6>
                </div>
              </div>
              <ul className="nav flex-column">
                {links}
              </ul>
            </div>
            <div className="mt-5 row justify-content-center">
              <a href="" target="_new" className="btn btn-link text-decoration-none">
                <span className="text-muted small text-white">
               
                </span>
              </a>
            </div>
          </div>
        </div>
        <div className="container side-bar-back" onClick={() => this.sideBarBack()}></div> */}

        <div>
          {tabList.id && tabList.subs ? (
            <>
              <div
                className={`text-right panel-hover ${borderBottom}`}
                onClick={() =>
                  this.setState({ openPanel: !this.state.openPanel })
                }
                >
                <i className={`icon ${tabList.iconClass} icon-20 ml-1`}></i>
                {tabList ? tabList.titleName : ''}
              </div>
              <div className={open}>
                  {tabList.subs.map((sub , k) => {
                      return (
                        <div className="mb-2 panel-hover" onClick={() => this.handleClick(sub.breadCrumbUri)}>{sub.titleName}</div>
                      )
                  })}
              </div>
            </>
          ) : (
            <div
              className="py-3 panel-hover bordered-bottom text-right"
              onClick={() => this.handleClick(tabList.breadCrumbUri)}
            >
              <i className={`icon ${tabList.iconClass} icon-20 ml-1`}></i>
              {tabList ? tabList.titleName : ''}
            </div>
          )}
        </div>
      </>
    );
  }
}

export default withRouter(Panel);
