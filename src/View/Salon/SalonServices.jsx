import React, { Component } from 'react';
import Menu from '../../Components/Menu';


class SalonServices extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div id="main-wrapper">
                <Menu
                    // routes={panelRoutes}
                    location={this.props.location}
                />
                <div id="content">
                    {/* {switchRoutes} */}
                </div>
            </div>
        );
    }
}

export default SalonServices;