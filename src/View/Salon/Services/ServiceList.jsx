import React, { Component } from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import { add_to_bascket } from "../../../redux/actions";

class ServiceList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false
    };
  }

  onMouseEnterHandler = () => {
    this.setState({
      hover: true
    });
  };

  onMouseLeaveHandler = () => {
    this.setState({
      hover: false
    });
  };

  handleActice = () => {
    this.props.handleActive(true);
    this.props.add_to_bascket(this.props.service);
  };

  render() {
    const { service } = this.props;
    const { hover } = this.state;
    let discount = 0,
      price = 0;
    const hoverClass = classnames({
      "h-75 text-center": true,
      "mouse-In": hover,
      "mouse-Out": !hover
    });

    return (
      <div className="card shadow-md mb-4">
        <img src="/img/1.jpg" className="card-img-top d-block" alt="" />
        <div className=" card-body text-center">
          <h5>
            <a className="text-dark text-5">{service.name}</a>
          </h5>
          <div
            className={hoverClass}
            onMouseEnter={this.onMouseEnterHandler}
            onMouseLeave={this.onMouseLeaveHandler}
          >
            {hover
              ? // service.description
                "از آنجایی که طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، پایان برند."
              : // service.description.substring(0, 30) + '...'
                "لورم ایپسوم یا طرح‌نما به متنی آزمایشی"}
          </div>

       

          <div className="d-flex ">
            <div className="col-lg-1 col-1 mt-4 text-center discount">
              {service.discount} %
            </div>
        
            <div className="col-lg-10 col-10 mb-0 mt-3 text-left text-2">
              <del className="text-2">{service.price} تومان</del> <br />
              <del>
             { (discount = Math.round(service.price * ((100 - service.discount) / 100))) }تومان </del>
            </div>
          </div>
         
          <p className="reviews mb-2"></p>
          <p className="text-danger mb-0"></p>
        </div>
        <div className="card-footer bg-transparent d-flex justify-content-center">
          <button
            className="btn btn-outline-search"
            onClick={this.handleActice}
          >
            افزودن به سبد خرید
          </button>
        </div>
      </div>
    );
  }
}

export default connect(null, { add_to_bascket })(ServiceList);
