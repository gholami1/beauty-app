import React, { Component } from 'react';
import ReactDatepicker from '../../../../Components/ReactDatepicker';

class OrderRegistrDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLabel: false,
    };
  }

  handleChangeDate = (value) => {
    const jDate = value.format('jYYYY-jMM-jDD HH:mm:ss');
    this.props.date(jDate);
    this.setState({
      date: jDate,
      fromDate: value,
      // showLabel: true,
    });
  };

  render() {
    const { fromDate, showLabel } = this.state;

    return (
      <div className="my-1 text-center position-relative">
        {showLabel ? (
          <label className="position-absolute position-date">
            <i className="icon icon-20 icon-dateTime"></i>
            <span
              className="text-right text-2 text-gray"
              onClick={() => this.setState({ showLabel: true })}
            >
              تاریخ مراجعه خود را وارد کنید
            </span>
          </label>
        ) : (
          ''
        )}
        <ReactDatepicker
          id="data"
          value={fromDate}
          onChange={this.handleChangeDate}
          className="calendar-Container size text-center rounded search-date pl-2 form-control"
        />
      </div>
    );
  }
}

export default OrderRegistrDate;
