import React, { Component } from 'react';
import { connect } from 'react-redux';
import DetailsBasket from './DetailsBasket';
import OrderRegistrDate from './OrderRegistrDate';
import classnames from 'classnames';
import ReactModal from 'react-modal';
import PreFactor from './PreFactor';
import { remove_all_bascket } from '../../../../redux/actions';

class BasketShop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      rotate: false,
      showModal: false,
      preFactor: false,
    };
  }

  // handleActive = act => {
  //   this.setState({
  //     active: act
  //   });
  // };

  componentDidMount() {
    if (this.props.isAuthenticated === false) {
      this.props.remove_all_bascket();
    }
  }

  handleClick = () => {
    this.setState({
      show: !this.state.show,
    });
  };

  handleRotate = () => {
    this.setState({
      rotate: !this.state.rotate,
    });
  };

  handlePreFactor = () => {
    this.setState({
      showModal: true,
    });
  };

  handleCloseModal = () => {
    this.setState({
      showModal: false,
    });
  };

  handleDate = (date) => {
    this.setState({
      date,
    });
  };

  render() {
    let sum_price = 0;
    const { buyBascket, active } = this.props;
    const { show, showModal, date } = this.state;

    console.log(this.props.isAuthenticated, active, 'buyBascket.length > 0');

    const toggle = classnames({
      'd-flex': true,
      'basket-cart': this.state.show,
      'non-border-button': !this.state.show,
    });

    const rotate = classnames({
      'pl-2 mt-1 arrow col-lg-1': true,
      rotate: this.state.show,
      'non-rotate': !this.state.show,
    });

    return (
      <div className="col-lg-12 col-12 p-0">
        <div className="col-lg-12 col-12 mt-4 p-0 shop-basket text-3">
          <div className="py-2 text-center basket-cart">
            <i className="icon icon-shopping-cart icon-24"></i>
            <span className="">سبد خرید</span>
          </div>
          <div className="py-2">
            {buyBascket.length > 0 ? (
              buyBascket.map((service) => {
                sum_price += service.price;
                return (
                  <div className="">
                    <DetailsBasket service={service} />
                  </div>
                );
              })
            ) : (
              <div className="col-12">
                <div className="non-basket pb-5 pt-5"></div>
                <p className="text-center text-basket">
                  سبد خرید شما خالی است.
                </p>
              </div>
            )}
          </div>
          {buyBascket.length > 0 ? (
            <div className="d-flex mb-2">
              <div className="col-lg-5 text-right">جمع کل</div>
              <div className="col-lg-7 text-left">{sum_price}تومان</div>
            </div>
          ) : (
            ''
          )}
        </div>

        {buyBascket.length > 0 ? (
          <div className="col-lg-12 col-12 details-basket mt-2 pt-2">
            <div className={toggle} onClick={this.handleClick}>
              <p className="col-lg-11 text-right text-3 pr-0 mb-1">
                انتخاب تاریخ و زمان رزرو
              </p>
              <div className={rotate} onClick={this.handleRotate}></div>
            </div>
            {show ? (
              <div>
                <div className="">
                  <OrderRegistrDate
                    date={this.handleDate}
                    dateTime={this.handleDateTime}
                  />
                </div>
              </div>
            ) : (
              ''
            )}
          </div>
        ) : (
          ''
        )}

        {buyBascket.length > 0 && date ? (
          <button
            className="col-lg-12 my-2 btn btn-outline-danger"
            onClick={this.handlePreFactor}
          >
            پیش فاکتور
          </button>
        ) : (
          ''
        )}
        <ReactModal
          className="ReactModal__Content"
          role="dialog"
          blockScroll={false}
          isOpen={showModal}
          onRequestClose={this.handleCloseModal}
          shouldCloseOnOverlayClick={true}
          style={{
            content: {
              overlfow: 'scroll',
              height: '100vh',
            },
            overlay: {
              backgroundColor: 'rgb(63,63,63,0.75)',
              zIndex: 10009,
            },
          }}
        >
          <PreFactor date={date} onClose={this.handleCloseModal} />
        </ReactModal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    buyBascket: state.buyBasket,
    isAuthenticated: state.isAuthenticated,
  };
};

export default connect(mapStateToProps, { remove_all_bascket })(BasketShop);
