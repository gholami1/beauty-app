import React, { useState, useEffect } from 'react';
import { Rating } from '@material-ui/lab';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import ProgressBar from '../../../../Components/ProgressBar';
import { get, post } from '../../../../services/api';
import SunEditor from 'suneditor-react';
import 'suneditor/dist/css/suneditor.min.css';
import { connect } from 'react-redux';
import ReactModal from 'react-modal';
import Login from '../../../Login/Login';
import moment from 'moment-jalaali';

const Comment = (props) => {
  const [star, setStar] = useState(0);
  const [comment, setComment] = useState([]);
  const [content, setContent] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [lastPage, setLastPage] = useState(null);

  useEffect(() => {
    getComment(currentPage);
  }, [currentPage]);

  const now = moment().format('YYYY/M/D');

  const getComment = (currentPage) => {
    if (props.salon_id) {
      try {
        const data = {
          shop_id: props.salon_id,
          type: 'shop',
          page: currentPage,
        };
        get('/comment', data, false).then(function (res) {
          setComment([...comment, ...res.data.data]);
          setCurrentPage(res.data.meta.current_page);
          setLastPage(res.data.meta.last_page);
        });
      } catch (err) {
        console.log('err', err);
      }
    }
  };

  const handleComment = async (e) => {
    e.preventDefault();
    try {
      let res = await post('/comment/create', { content }, true);
      // handleRes(res.data.data);
    } catch (err) {
      console.log('err', err);
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const moreComment = () => {
    setCurrentPage((prevCurrentPage) => prevCurrentPage + 1);
  };

  console.log(comment, now, 'com');
  let create;
  return (
    <div className="border-box my-5 text-2">
      <div className="d-flex justify-content-center my-4">
        <span className="text-center text-light font-weight-bold pt-1 box-rate">
          4.6
        </span>
        <p className="mb-0 mx-2">
          <Rating
            name="customized-empty"
            readOnly={true}
            value={star}
            precision={0.2}
            emptyIcon={<StarBorderIcon fontSize="inherit" />}
          />
        </p>
        <span className="mr-1">از مجموع ۱۰۵۱ رای</span>
      </div>
      <div className="row justify-content-center">
        <div className="d-flex justify-content-center col-12 col-lg-12">
          <i className="icon icon-20 icon-happy ml-2"></i>
          <div className="col-6 col-lg-4 mt-2">
            <ProgressBar bgcolor={'blue'} completed={95} />
          </div>
          <span className="mr-2">%۹۱.۹ راضی</span>
        </div>
        <div className="d-flex justify-content-center col-12 col-lg-12 mt-2">
          <i className="icon icon-20 icon-straight ml-2"></i>
          <div className="col-6 col-lg-4 mt-2">
            <ProgressBar bgcolor={'yellow'} completed={3} />
          </div>
          <span className="mr-2">%۰ بدون نظر</span>
        </div>
        <div className="d-flex justify-content-center col-12 col-lg-12 mt-2">
          <i className="icon icon-20 icon-sad ml-2"></i>
          <div className="col-6 col-lg-4 mt-2">
            <ProgressBar bgcolor={'red'} completed={2} />
          </div>
          <span className="mr-2">%۸.۱ ناراضی</span>
        </div>
      </div>
      {comment
        ? comment.map((item, k) => {
            create = item.created_at;
            console.log(create, 'created_at');
            return (
              <div
                id={item.id}
                key={k}
                className="d-flex justify-content-center col-lg-10 mx-auto pt-2 text-2 text-right my-2"
              >
                <div className="col-5 col-lg-2">
                  <span className="">سحر</span>
                  <br />
                  <span className="">۱۰ روز پیش</span>
                </div>
                <div className="col-7 col-lg-10 text-justify">
                  <span className=""></span>
                  <div className="mb-3">{item.context}</div>
                </div>
              </div>
            );
          })
        : ''}
      {currentPage < lastPage ? (
        <div className="d-flex justify-content-center my-4">
          <button
            className="btn btn-danger text-2"
            onClick={() => moreComment()}
          >
            + نظرات بیشتر
          </button>
        </div>
      ) : null}
      {props.isAuthenticated ? (
        <div className="text-right col-lg-12 mb-3">
          <form onSubmit={handleComment}>
            <SunEditor
              name="myEditor"
              className=""
              placeholder="لطفا نظر خود را ثبت کنید."
              onChange={(val) => setContent(val)}
            />
            <div className="d-flex justify-content-center mt-4">
              <input
                className="btn btn-danger px-5 font-size-14"
                type="submit"
                value="ثبت نظر"
              />
            </div>
          </form>
        </div>
      ) : (
        <div className="text-center mb-3">
          <span> لطفا برای ثبت نظر </span>
          <span
            className="cursor-pointer show-login"
            onClick={() => setShowModal(true)}
          >
            ثبت نام
          </span>
          <span className=""> کنید. </span>
          <ReactModal
            className="ReactModal__Content"
            role="dialog"
            blockScroll={false}
            isOpen={showModal}
            onRequestClose={handleCloseModal}
            shouldCloseOnOverlayClick={true}
            style={{
              content: {
                overlfow: 'scroll',
                height: '100vh',
              },
              overlay: {
                backgroundColor: 'rgb(63,63,63,0.75)',
                zIndex: 10009,
              },
            }}
          >
            <Login onClose={handleCloseModal} />
          </ReactModal>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
  };
};

export default connect(mapStateToProps, null)(Comment);
