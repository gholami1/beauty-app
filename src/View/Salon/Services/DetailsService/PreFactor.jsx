import React, { Component } from "react";
import { connect } from "react-redux";
import { post , createFactor } from "../../../../services/api";
import store from "../../../../redux/store";

class PreFactor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      payment: 1,
      api_token: null
    };
  }

  componentDidMount() {}

  handleChange = e => {
    const target = e.target;
    const payment = target.type === "checkbox" ? target.checked : target.value;
    this.setState({
      payment
    });
  };

  createFactor = async () => {
    try {
    let service = this.props.buyBascket.map(service => {
      return service.id;
    });
    const factor = {
      type_cash: this.state.payment,
      date_time: this.props.date,
      services: service.toString()
    };
    
    let res =await post("/factors/create_factors", factor, true);
      // .then(
        // function(res) {
          console.log(res , 'resssssss');
          // this.setState({
          //   result: res.data
          // });
        // }.bind(this)
      // )
    } catch(err) {
        console.log("err", err);
      };
  };

  render() {
    const { buyBascket, date, addSalon } = this.props;
    const { payment, site_title } = this.state;
    console.log(buyBascket, "buyBascket", "addSalon");
    let discount = 0,
      discounts = 0,
      count_price = 0;
    return (
      <div id="login-signup">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content p-sm-3 bg-white">
            <div className="modal-body">
              <a
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => this.props.onClose()}
              >
                <i className="icon icon-close icon-20"></i>
              </a>
              <h5 className="text-right mb-5">پیش فاکتور</h5>

              <div className="position-relative mb-5">
                <p className="position-absolute bg-white text-right details-salon">
                  جزئیات سالن
                </p>
                <div className="detail-salon">
                  <div className="d-flex my-1 text-center">
                    <div className="col-lg-4">سالن</div>
                    <div className="col-lg-4">توضیحات</div>
                    <div className="col-lg-4">آدرس</div>
                  </div>

                  <div className="d-flex text-center">
                    <div className="col-lg-4 col-4">{addSalon.name}</div>
                    <div className="col-lg-4 col-4">{addSalon.context}</div>
                    <div className="col-lg-4 col-4">{addSalon.address}</div>
                  </div>
                </div>
              </div>

              <div className="position-relative mb-5">
                <p className="text-right details-salon position-absolute bg-white">
                  جزئیات سرویس
                </p>
                <div className="detail-salon">
                  <div className="d-flex my-1 text-center">
                    <div className="col-lg-3">سرویس</div>
                    <div className="col-lg-3">توضیحات سرویس</div>
                    <div className="col-lg-3">قیمت</div>
                    <div className="col-lg-3">درصد تخفیف</div>
                  </div>

                  {buyBascket.map(service => {
                    discount = service.price * ((100 - service.discount) / 100);
                    discounts += service.price * (service.discount / 100);
                    count_price += discount;

                    return (
                      <div className="d-flex text-center">
                        <div className="col-lg-3 col-3">{service.name}</div>
                        <div className="col-lg-3 col-3">
                          {service.description}
                        </div>
                        <div className="col-lg-3 col-3">{service.price}</div>
                        <div className="col-lg-3 col-3">{discount}</div>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="d-flex position-relative">
                <p className="text-right details-salon position-absolute bg-white"> جزئیات پرداخت</p>
                <div className="d-flex col-lg-12 px-0 detail-salon my-1">
                  <div className="col-lg-12 d-flex px-0">
                      <div className="col-lg-6 text-right py-1">مجموع تخفیف</div>
                      <div className="col-lg-6 text-right py-1">جمع کل</div>
                  </div>
                </div>
                </div>
                  <div className="d-flex">
                    <div className="col-lg-12">
                    <h5 className="text-right">نوع پرداخت</h5>
                    <div className="d-flex">
                      <div className="custom-control custom-radio col-lg-6 px-0 text-right">
                        <input
                          type="radio"
                          id="customRadio1"
                          name="customRadio"
                          class="custom-control-input"
                          value={"0"}
                          defaultChecked
                          onChange={this.handleChange}
                        />
                        <label class="custom-control-label" for="customRadio1">
                          پرداخت نقدی
                        </label>
                      </div>
                      <div className="custom-control col-lg-6 custom-radio text-right">
                        <input
                          type="radio"
                          id="customRadio2"
                          name="customRadio"
                          class="custom-control-input"
                          value={"1"}
                          onChange={this.handleChange}
                        />
                        <label class="custom-control-label" for="customRadio2">
                          پرداخت اینترنتی
                        </label>
                      </div>
                      </div>
                    </div>
                  </div>

              <div className="">
                <button
                  className="btn btn-outline-danger"
                  onClick={this.createFactor}
                >
                  ثبت اولیه
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    buyBascket: state.buyBasket,
    addSalon: state.addSalon
  };
};
export default connect(mapStateToProps, null)(PreFactor);
