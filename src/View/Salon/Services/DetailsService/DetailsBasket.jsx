import React, { Component } from "react";
import { connect } from "react-redux";
import { remove_from_bascket } from "../../../../redux/actions";

class DetailsBasket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false
    };
  }

  onMouseEnterHandler = () => {
    this.setState({
      hover: true
    });
  };

  onMouseLeaveHandler = () => {
    this.setState({
      hover: false
    });
  };

  render() {
    const { service, remove_from_bascket } = this.props;
    return (
      <div>
        <div className="d-flex pb-2">
          {this.state.hover ? (
            <div className="" onMouseEnter={this.onMouseEnterHandler} onMouseLeave={this.onMouseLeaveHandler} onClick={() => remove_from_bascket(service)}>
              <i className="icon icon-remove icon-14"></i>
            </div>
          ) : ""}
          <div className="col-lg-4 text-right remove-product" onMouseEnter={this.onMouseEnterHandler} onMouseLeave={this.onMouseLeaveHandler} >
            {service.name}
          </div>
          <div className="col-8 text-left">
            <span className="">{service.price}</span>
            <span className="">تومان</span>
          </div>
        </div>
        {/* <div className="d-flex">
          <div className="col-lg-9 text-right">جمع کل</div>
          <div className="col-lg-3 text-right">15</div>
        </div> */}
      </div>
    );
  }
}

export default connect(null, { remove_from_bascket })(DetailsBasket);
