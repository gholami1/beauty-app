import React, { Component } from 'react';
import { get } from '../../../services/api';
import ServiceList from './ServiceList';
import { Tabs, Tab } from 'react-bootstrap';
import Comment from '../Services/DetailsService/Comments';
import Description from './DetailsService/Description';
import BasketShop from './DetailsService/BasketShop';
import { connect } from 'react-redux';
import BreadCrumb from '../../../Components/BreadCrumb';
import Loader from 'react-loader-spinner';

class Services extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
    this.getServices();
  }

  componentDidMount() {
    // console.log(this.props , 'props');
    this.setState({
      loaderSpin : this.props.location.state.loaderSpin,
    })
  }

  getServices = async () => {
    try {
      const data = this.props.match.params.param;
      let res = await get('/services', { name_en: data }, false);
      this.setState({
        SalonServices: res.data.data,
        loaderSpin: false,
      });
    } catch (err) {
      console.log(err, 'err');
    }
  };

  handleActive = (act) => {
    this.setState({
      active: act,
    });
  };

  render() {
    const { SalonServices, active, loaderSpin } = this.state;
    const { buyBascket } = this.props;
    console.log(loaderSpin , 'loaderSpin');
    return (
      <div className="">
        {
          loaderSpin ?
            <div className="d-flex justify-content-center mt-5">
              <Loader type="TailSpin" color="#00BFFF" height={80} width={80} />
            </div>
            :
            <>
              <div className="row">
                <div className="col-lg-12 col-12">
                  <BreadCrumb />
                </div>
              </div>
              <div className="row mt-4 mx-0">
                <div className="col-lg-9 col-12 justify-content-between">
                  <div className="row">
                    <Tabs className="nav mt-1 mr-lg-3 col-lg-12 col-12 pr-0 nav-tabs text-13">
                      <Tab
                        className="col-lg-12 col-12 mr-0 nav-item"
                        eventKey="جزئیات سرویس"
                        title="جزئیات سرویس"
                      >
                        <div className="row">
                          {SalonServices
                            ? SalonServices.map((service, key) => {
                                return (
                                  <div className="col-lg-3 col-md-3 col-sm-3 col-12 my-4">
                                    <ServiceList
                                      service={service}
                                      key={key}
                                      handleActive={this.handleActive}
                                    />
                                  </div>
                                );
                              })
                            : null}
                        </div>
                      </Tab>
                      <Tab className="nav-item" eventKey="نظرات" title="نظرات">
                        <Comment salon_id={this.props.location.state.salon.id} />
                      </Tab>
                      <Tab className="nav-item" eventKey="توضیحات" title="توضیحات">
                        <Description salon={this.props.location.state.salon} />
                      </Tab>
                    </Tabs>
                  </div>
                </div>
                <div className="col-lg-3 col-12">
                  <div className="row mt-5 ml-4 mr-2">
                    <BasketShop active={active} />
                  </div>
                </div>
              </div>
            </>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    buyBascket: state.buyBascket,
  };
};

export default connect(mapStateToProps, null)(Services);
