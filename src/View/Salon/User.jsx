import React from "react";
import { UserContext } from "../../Context/UserProvider";

function User() {
    return (
        <UserContext.Consumer>
            {({ user , logout }) => user.loggedIn ? ( <Profile name={user.name} handleLogout={logout} /> ) : ( <Login name={user.name} /> ) }
        </UserContext.Consumer>
    )
}

function Profile(props) {
    return (
        <>
            <p>Hello ,gggggg {props.username}</p>
            <button onClick={props.handleLogout} className="btn btn-danger">Logout</button>
        </>
    )
}

function Login(props){
    return (
        <>
            <p>Please Login , {props.name}</p>
            <button className="btn btn-danger">Login</button>
        </>
    )
}

export default User