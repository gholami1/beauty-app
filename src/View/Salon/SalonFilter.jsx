import React, { Component } from 'react';
import classnames from 'classnames';
import { post } from '../../services/api';
import { connect } from 'react-redux';
import { add_to_filter } from '../../redux/actions/index';

class SalonFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      showBox: true,
      showed: true,
      showOrder: true,
      showFilter: true,
      gender: 0,
      sort: null,
      discount: null,
    };
  }

  handleToggle = () => {
    this.setState({
      show: !this.state.show,
    });
  };

  handleShow = () => {
    this.setState({
      showBox: !this.state.showBox,
    });
  };

  handleOrderOpen = () => {
    this.setState({
      showOrder: !this.state.showOrder,
    });
  };

  handleToggleShow = () => {
    this.setState({
      showed: !this.state.showed,
    });
  };

  handleToggleFilter = () => {
    this.setState({
      showFilter: !this.state.showFilter,
    });
  };

  handleFilter = (e) => {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState(
      {
        [target.name]: value,
      },
      () =>
        this.props.resultData({
          gender: this.state.gender,
          sort: this.state.sort,
          discount: this.state.discount,
        })
      //   () =>
      //     this.props.add_to_filter({
      //       gender: this.state.gender,
      //       sort: this.state.sort,
      //       discount: this.state.discount,
      //     })
    );
  };

  render() {
    const toggle = classnames({
      'mt-2': true,
      show: this.state.show,
      hide: !this.state.show,
    });

    const rotate = classnames({
      'pl-2 arrow': true,
      rotate: this.state.show,
      'non-rotate': !this.state.show,
    });

    const orderOpen = classnames({
      'pl-2 arrow': true,
      rotate: this.state.showOrder,
      'non-rotate': !this.state.showOrder,
    });

    const orderToggle = classnames({
      'mt-2': true,
      show: this.state.showOrder,
      hide: !this.state.showOrder,
    });

    const filterBy = classnames({
      'pl-2 arrow': true,
      rotate: this.state.showFilter,
      'non-rotate': !this.state.showFilter,
    });

    const filterToggle = classnames({
      'mt-2': true,
      show: this.state.showFilter,
      hide: !this.state.showFilter,
    });

    return (
      <div>
        <div className="filter text-2 col-12 col-lg-12 p-2 rounded text-right">
          <div className="row">
            <div className="text-4 font-weight-bold col-lg-10 col-10">
              جنسیت
            </div>
            <div className={rotate} onClick={this.handleToggle}></div>
          </div>
          <div className={toggle}>
            <div className="custom-control custom-radio">
              <input
                type="radio"
                id="women"
                name="gender"
                className="custom-control-input"
                value="0"
                checked={this.state.gender === '0'}
                onChange={this.handleFilter}
              />
              <label
                className="custom-control-label custom-label-filter"
                htmlFor="women"
              >
                زن
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input
                type="radio"
                id="male"
                name="gender"
                className="custom-control-input"
                value="1"
                checked={this.state.gender === '1'}
                onChange={this.handleFilter}
              />
              <label
                className="custom-control-label custom-label-filter"
                htmlFor="male"
              >
                مرد
              </label>
            </div>
          </div>
        </div>
        <div className="col-12 col-lg-12 mt-3 p-2 text-2 rounded filter text-right">
          <div className="row">
            <div className="text-4 font-weight-bold col-lg-10 col-10">
              مرتب کردن براساس
            </div>
            <div className={orderOpen} onClick={this.handleOrderOpen}></div>
          </div>
          <div className={orderToggle}>
            <div className="custom-control custom-radio">
              <input
                type="radio"
                id="rate"
                name="sort"
                className="custom-control-input"
                value="rate"
                onChange={this.handleFilter}
              />
              <label
                className="custom-control-label custom-label-filter"
                htmlFor="rate"
              >
                بیشترین امتیاز
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input
                type="radio"
                id="new"
                name="sort"
                className="custom-control-input"
                value="latest"
                onChange={this.handleFilter}
              />
              <label
                className="custom-control-label custom-label-filter"
                htmlFor="new"
              >
                جدیدترین ها
              </label>
            </div>
          </div>
        </div>
        <div className="col-12 col-lg-12 text-2 mt-3 p-2 rounded filter text-right">
          <div className="row">
            <div className="text-4 font-weight-bold col-lg-10 col-10">
              فیلتر
            </div>
            <div className={filterBy} onClick={this.handleToggleFilter}></div>
          </div>
          <div className={filterToggle}>
            <div className="custom-control custom-radio">
              <input
                type="radio"
                id="off"
                name="discount"
                className="custom-control-input"
                value="off"
                onChange={this.handleFilter}
              />
              <label
                className="custom-control-label custom-label-filter"
                htmlFor="off"
              >
                با تخفیف
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input
                type="radio"
                id="coupons"
                name="discount"
                className="custom-control-input"
                value="coupons"
                onChange={this.handleFilter}
              />
              <label
                className="custom-control-label custom-label-filter"
                htmlFor="coupons"
              >
                کوپن
              </label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null, { add_to_filter })(SalonFilter);
