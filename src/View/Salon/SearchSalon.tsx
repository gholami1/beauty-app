// import React, { Component } from 'react';
// import { withRouter } from 'react-router-dom';
// import { post } from '../../services/api';

// class SearchSalon extends Component {
//   constructor() {
//     super();
//     this.state = {
//       female: '1',
//       men: '0',
//       lat: null,
//       lng: null,
//     };
//   }

//   componentWillReceiveProps(nextProps) {
//     this.setState({
//       sex: nextProps.data.sex,
//       city: nextProps.data.city,
//       Neighborhood: nextProps.data.street,
//       Services: nextProps.data.service,
//       salon: nextProps.data.shop,
//     });
//   }

//   handleSearch = (e) => {
//     const target = e.target;
//     const value = target.type === 'checkbox' ? target.checked : target.value;
//     this.setState({
//       [target.name]: value,
//     });
//   };

//   search = () => {
//     let searchData = {};
//     searchData = {
//       sex: this.state.value ? this.state.value : this.state.sex,
//       city: this.state.city,
//       street: this.state.Neighborhood,
//       service: this.state.Services,
//       shop: this.state.salon,
//     };

//     console.log('searchData' , searchData);

//     let stringSearch = '';
//     for (var key in searchData) {
//       stringSearch += `${searchData[key]}&`;
//     }
//     this.props.history.push(`/home/salon/listService/${stringSearch}`);
//     this.props.resultData(searchData);
//   };

//   searchNearBy = async () => {
//     await navigator.geolocation.getCurrentPosition(
//       (position) =>
//         this.setState(
//           {
//             lat: position.coords.latitude,
//             lng: position.coords.longitude,
//           },
//           () => this.handleNearby()
//         ),
//       (err) => console.log(err)
//     );
//   };

//   handleNearby = async () => {
//     console.log(this.state.lat, this.state.lng, 'lng');
//     const data = {
//       lat: this.state.lat,
//       lng: this.state.lng,
//       count: 6,
//     };
//     try {
//       let res = await post('/nearstShop', data, false);
//       console.log(data, 'data');
//       this.props.dataNearby(res.data.data);
//     } catch (err) {
//       console.log('err', err);
//     }
//   };

//   render() {
//     const {
//       sex,
//       city,
//       Neighborhood,
//       Services,
//       salon,
//       female,
//       men,
//     } = this.state;
//     return (
//       <div className="search col-lg-12 mx-auto col-md-11 col-sm-9 col-12 search-wrap py-2 mt-5">
//         <div className="row text-3 pr-4 pb-2">
//           <div className="custom-control custom-radio">
//             <input
//               id="female"
//               name="sex"
//               className="custom-control-input"
//               required
//               type="radio"
//               onChange={this.handleSearch}
//               value={female}
//               checked={female === sex}
//             />
//             <label className="custom-control-label text-dark" htmlFor="female">
//               زن
//             </label>
//           </div>
//           <div className="custom-control custom-radio mr-1">
//             <input
//               id="men"
//               name="sex"
//               className="custom-control-input"
//               required
//               type="radio"
//               onChange={this.handleSearch}
//               value={men}
//               checked={men === sex}
//             />
//             <label className="custom-control-label text-dark" htmlFor="men">
//               مرد{' '}
//             </label>
//           </div>
//         </div>
//         <div className="row pt-3 justify-content-around no-gutters">
//           <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
//             <input
//               id="city"
//               name="city"
//               type="text"
//               className="form-control input-search text-2"
//               required
//               placeholder="شهر"
//               value={city}
//               onChange={this.handleSearch}
//             />
//           </div>
//           <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
//             <input
//               id="Neighborhood"
//               name="Neighborhood"
//               type="text"
//               className="form-control input-search text-2"
//               required
//               placeholder="محله"
//               value={Neighborhood}
//               onChange={this.handleSearch}
//             />
//           </div>
//           <div className="col-lg-1 col-md-1 col-sm-1 col-5 form-group">
//             <input
//               id="Services"
//               name="Services"
//               type="text"
//               className="form-control input-search text-2"
//               required
//               placeholder="خدمات"
//               value={Services}
//               onChange={this.handleSearch}
//             />
//           </div>
//           <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
//             <input
//               id="salon"
//               name="salon"
//               type="text"
//               className="form-control input-search text-2"
//               required
//               placeholder="سالن"
//               value={salon}
//               onChange={this.handleSearch}
//             />
//           </div>
//           <div className="col-lg-1 col-md-2 col-sm-2 col-11 d-flex justify-content-center form-group">
//             <button
//               className="btn w-100 text-2 px-0 searchBtn"
//               type="submit"
//               onClick={this.search}
//             >
//               <span className=""> جستجو </span>
//               <i className="icon icon-14 icon-search"></i>
//             </button>
//           </div>
//           <div className="col-lg-3 col-md-2 col-sm-2 col-11 d-flex justify-content-center form-group">
//             <button
//               className="btn w-100 text-3 btn-danger"
//               type="submit"
//               onClick={this.searchNearBy}
//             >
//               جستجو براساس نزدیک ترینها
//               <i className="icon icon-14 icon-cursor mr-1"></i>
//             </button>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }
// export default withRouter(SearchSalon);


import React, { useState } from 'react';
import { withRouter , RouteComponentProps } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import { post } from '../../services/api';


type ListProps = {
  data: any,
  dataNearby: any,
  resultData: any,
};

interface dataSearch {
  sex: any,
  city: string,
  Neighborhood: string,
  services: string,
  salon: string,
  [key: string]: string;
};

const SearchSalon: React.FC<ListProps> = ({ data , dataNearby , resultData }) => {

    let dataserch : string;
    let history = useHistory();
    const [latLng , setLatLng] = useState({ lat: 0 , lng: 0 });
    const [searchData , setSearchData] = useState({ sex: data.sex ? data.sex :'1', city : data.city ? data.city : '', Neighborhood : data.Neighborhood ? data.Neighborhood : '', Services : data.Services ? data.Services :'', salon: '',  female : '', men : '' });
   
  // componentWillReceiveProps(nextProps) {
  //   this.setState({
  //     sex: nextProps.data.sex,
  //     city: nextProps.data.city,
  //     Neighborhood: nextProps.data.street,
  //     Services: nextProps.data.service,
  //     salon: nextProps.data.shop,
  //   });
  // }

  const handleSearch = (e : any) => {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    setSearchData({
      ...searchData ,[target.name]: value,
    });
  };

  const search = () => {

    let search : Partial<dataSearch> = {
      "sex": searchData.sex,
    };

  if(searchData.city) {
    search = {
        "city": searchData.city,
        ...search,
    }
  }
  if(searchData.Neighborhood) {
      search = {
          "street": searchData.Neighborhood,
          ...search,
      }
  }
  if(searchData.Services) {
    search = {
      "service": searchData.Services,
      ...search,
    }
  }
  if(searchData.salon) {
    search = {
      "salon": searchData.salon,
      ...search,
    }
  }
  
  let stringSearch : string = '';
  for (var key in search) {
    stringSearch += `${search[key]}&`;
  }
    // console.log('searchData' , searchData);
    dataserch = stringSearch.substring(0, stringSearch.length - 1);
    // setIsDone(true);
    history.push(`/home/salon/listService/${dataserch}` , { search: search , loaderSpin: true });
    resultData(search);
  };

  console.log('dataout' , data);
  

  const searchNearBy = async () => {
    await navigator.geolocation.getCurrentPosition(
      (position) =>
      setLatLng(
          {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          },
          // this.handleNearby()
        ),
      (err) => console.log(err)
    );
  };

  const handleNearby = async () => {
    // console.log(this.state.lat, this.state.lng, 'lng');
    const data = {
      lat: latLng.lat,
      lng: latLng.lng,
      count: 6,
    };
    try {
      let res = await post('/nearstShop', data, false);
      console.log(data, 'data');
      dataNearby(res.data.data);
    } catch (err) {
      console.log('err', err);
    }
  };
  
    return (
      <div className="search col-lg-12 mx-auto col-md-11 col-sm-9 col-12 search-wrap py-2 mt-5">
        <div className="row pt-3 justify-content-around no-gutters">
          <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
            <input
              id="city"
              name="city"
              type="text"
              className="form-control input-search text-2"
              required
              placeholder="شهر"
              value={searchData.city}
              onChange={handleSearch}
            />
          </div>
          <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
            <input
              id="Neighborhood"
              name="Neighborhood"
              type="text"
              className="form-control input-search text-2"
              required
              placeholder="محله"
              value={searchData.Neighborhood}
              onChange={handleSearch}
            />
          </div>
          <div className="col-lg-1 col-md-1 col-sm-1 col-5 form-group">
            <input
              id="Services"
              name="Services"
              type="text"
              className="form-control input-search text-2"
              required
              placeholder="خدمات"
              value={searchData.Services}
              onChange={handleSearch}
            />
          </div>
          <div className="col-lg-2 col-md-2 col-sm-2 col-5 form-group">
            <input
              id="salon"
              name="salon"
              type="text"
              className="form-control input-search text-2"
              required
              placeholder="سالن"
              value={searchData.salon}
              onChange={handleSearch}
            />
          </div>
          <div className="col-lg-1 col-md-2 col-sm-2 col-11 d-flex justify-content-center form-group">
            <button
              className="btn w-100 text-2 px-0 searchBtn"
              type="submit"
              onClick={search}
            >
              <span className=""> جستجو </span>
              <i className="icon icon-14 icon-search"></i>
            </button>
          </div>
          <div className="col-lg-3 col-md-2 col-sm-2 col-11 d-flex justify-content-center form-group">
            <button
              className="btn w-100 text-3 btn-danger"
              type="submit"
              onClick={searchNearBy}
            >
              جستجو براساس نزدیک ترینها
              <i className="icon icon-14 icon-cursor mr-1"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
export default SearchSalon;

