import React, { useEffect , useState } from 'react';
import SalonFilter from './SalonFilter';
import { get , post } from '../../services/api';
import SalonList from './SalonList';
import SearchSalon from './SearchSalon';
import BreadCrumb from '../../Components/BreadCrumb';
import { connect } from 'react-redux';
import Spinner from '../../Components/Spinner';
import { useLocation } from 'react-router-dom';

interface LocationState {
    pathname: string;
    search: any;
    loaderSpin: boolean;
}


const Salon : React.FC = (props : any) => {
  const [fav , setFav] = useState([]);
  const [searchData , setSearchData] = useState({ sex:'' , city: '', street: '', service: '', shop: ''});
  const [salonList , setSalonList] = useState([]);
  const [loaderSpin , setLoaderSpin] = useState(true);
  const [isDone, setIsDone] = useState(false);

  let location = useLocation<LocationState>();

  // constructor(props) {
  //   super(props);
  //   this.auth = null;
  // }

  useEffect(() => {
    getFavorites();
    if (location.pathname) {
      const dataSerch : Array<string> = props.match.params.stringSrch.split('&');
      setSearchData(location.state.search);
      setLoaderSpin(location.state.loaderSpin);
      // handleResultData(location.state.search);
      console.log('location.state.search' , location.state.search);
      
      handleResultData(location.state.search);
    }
  },[])


      const handleResultData = async (search : any) => {
      try {
        let res = await post('/shop/search', search, false);
        console.log('res.dataa' , res.data.data);
        if(res.status === 200) {
          setSalonList(res.data.data);
          setLoaderSpin(false);
        }
      } catch (err) {
        console.log('err', err);
      }
    };

  const getFavorites = async () => {
    if (props.isAuthenticated) {
      try {
        let res = await get('/favorite', {}, true);
        setFav(res.data.data);
      } catch (err) {
        console.log('err', err);
      }
    }
  };

  const handleDataNearby = (data : any) => {
    setSalonList(data);
  };

  const filter = (filterData : any) => {
    const Filter = { ...searchData, ...filterData };
    // () => handleResultData(Filter);
  };

  // const handleSearchData = (data : any) => {
  //   console.log('data' , data);
  //   // handleResultData(data);
  //   setSearchData(data);
  //   handleResultData(data);
  // }

    return (
      <div className="container box-flight">
        {loaderSpin ?
        <Spinner 
          open={loaderSpin}
        />
        :
          <>
            <BreadCrumb />
            <SearchSalon
              data={searchData}
              resultData={handleResultData}
              // setSearchData={setSearchData}
              dataNearby={handleDataNearby}
            />
            <div className="row">
              <div className="col-lg-3 mt-4 bg-light">
                <div className="bg-light shadow-md rounded py-4">
                  <SalonFilter resultData={filter} />
                </div>
              </div>
              <div className="col-lg-9 mt-1">
                <div className="row">
                  <div className="col-lg-12 mt-4">
                    <div className="bg-light shadow-md rounded py-4 text-center box-salon">
                      {salonList.length >= 1 ?
                        salonList.map((prop : any, key) => {
                          let favorite = false;
                          fav && fav.forEach((item : any) => {
                            if (item.id === prop.id) {
                              favorite = true;
                            }
                          });
                          return <div className="col-lg-4 col-12 mb-5" key={key}>
                            <SalonList salonList={prop} favIcon={favorite} />
                          </div>
                        })
                      : 'نتیجه یافت نشد'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        }
      </div>
    );
}

const mapStateToProps = (state : any) => {
  return {
    itemFilter: state.itemFilter,
    isAuthenticated: state.isAuthenticated,
  };
};

export default connect(mapStateToProps, null)(Salon);