import React, { useEffect, useState } from 'react';
import { Rating } from '@material-ui/lab';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { add_to_salon } from '../../redux/actions/index';
import classNames from 'classnames';
import { post } from '../../services/api';

type ListProps = {
  salonList: any,
  favIcon: boolean,
};


const SalonList : React.FC<ListProps> = ({ salonList , favIcon}) => {
  let history = useHistory();
  const [favicon , setFavicon] = useState(false);
 
  useEffect(() => {
    setFavicon(favIcon)
  },[])
  
  const handleDetailsList = (salon : any) => {
    console.log('salon' , salon);
    const salon_name = salon.name_en;
    // props.add_to_salon(salonList);
    history.push(`/services/salonServices/${salon_name}`, {
      salon: salon , loaderSpin: true
    });
  };

  const handleFavIcon = async (id : number) => {
    try {
      let res = await post('/favorite/toggle_set', { shop_id: id }, true);
      console.log('res' , res.data.data);
      setFavicon(!favicon);
    } catch (err) {
      console.log('err', err);
    }
  };

  // console.log('res' , salonList , favicon);
  // const handleFavorire = async (id) => {
  //   // if (this.state.fav_icon) {
  //   //   // const fav_icon = {
  //   //   //   id: this.props.salonList,
  //   //   // };
  //   // }
  // };

    const handleFavIc = classNames({
      'icon icon-18 position-absolute favorites': true,
      'icon-heart': favicon,
      'icon-heart-white': !favicon,
    });
    
    return (
      <div className="list-salon text-3">
        {/* <div className=""> */}
        <div className="col-lg-12 box-img p-0 position-relative">
          <i
            className={handleFavIc}
            onClick={() => handleFavIcon(salonList.id)}
          ></i>
          <img
            className="img-fluid img-h w-100"
            src={
              salonList.images
                ? `http://localhost:8000${salonList.images}`
                : '/img/1.jpg'
            }
          />
        </div>
        {/* <div className="col-lg-6"> */}
        {/* <img className="img-tag img-fluid" src='' /> */}
        {/* </div> */}
        {/* </div> */}
        <div className="box-item col-lg-12 px-0">
          <p className="mt-2 mb-2">{salonList.name}</p>
          <p className="mb-0">
            <Rating
              name="customized-empty"
              readOnly={true}
              value={salonList.rate}
              precision={0.2}
              emptyIcon={<StarBorderIcon fontSize="inherit" />}
            />
          </p>
          <p className="mt-1 salon-address">{salonList.address}</p>
        </div>
        <div className="box-button my-2">
          <button
            className="btn btn-outline-search text-3"
            onClick={() => handleDetailsList(salonList)}
          >
            نمایش جزئیات بیشتر
          </button>
        </div>
      </div>
    );
}

export default connect(null, { add_to_salon })(SalonList);