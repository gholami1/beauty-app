import React, { Component } from 'react';
import { get , getFactors } from "../../services/api";
import moment from "moment-jalaali";

const titles = [
    {
        label: "نام سالن"
    },
    {
        label: "خدمات"
    },
    {
        label: "قیمت اصلی"
    },
    {
        label: "قیمت نهایی"
    },
    {
        label: "تخفیف سرویس"
    },
    {
        label: "تخفیف سایت"
    },
    {
        label: "تاریخ"
    },
    {
        label: "نوع پرداخت"
    }
  ];

class FutureFactors extends Component {
    constructor(props) {
        super(props);
        this.state = {
            typeFactor: "future_factors",
        }
        this.getFactors();
    }

    getFactors = async () => {
        try {
            let res = await get(`/factors/${this.state.typeFactor}`, {}, true);
              this.setState({
                factors: res.data.data
              });
        } catch (err) {
            console.log("err", err);
        };
    }
        
    
    render() {
        const { factors } = this.state;
        
        return (
            <div className="col-lg-12 border-box my-5">
                <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
                    <div className="col-lg-2 col-md-2 col-sm-3 col-4 title-box border-box position-absolute d-flex justify-content-center">
                        <i className="icon icon-creditCard text-dark icon-64 icon-pass"></i>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-lg-12 pb-5">
                        <h5 className="text-center title-pass">سفارش های پیش رو</h5>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 text-center font-size-13">
                        <table class="table table-responsive table-bordered">
                        <thead>
                            <tr>
                                {titles ?
                                titles.map((col) => {
                                    return(
                                    <th scope="col">{col.label}</th>
                                    )
                                })
                                :  
                                ""
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {
                            factors ? 
                                factors.map((factor , k) => {
                                const dateTime = moment(factor.date).format("jYYYY-jMM-jDD HH:mm:ss");

                                return(
                                    <tr className="">
                                        <td>{factor.shop_id}</td>
                                        <td>{factor.services}</td>
                                        <td>{factor.price_primary}</td>
                                        <td>{factor.price_final}</td>
                                        <td>{factor.service_discount}</td>
                                        <td>{factor.site_discount}</td>
                                        <td>
                                            {dateTime}
                                        </td>
                                        <td>
                                            {factor.type_cash === 0 ? 'پرداخت اینترنتی' : 'پرداخت نقدی'}
                                        </td>
                                    </tr>
                                   ) 
                                })
                               : 
                             ""
                             } 
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default FutureFactors;