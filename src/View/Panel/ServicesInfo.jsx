import React, { useState, useEffect } from "react";
import { get ,getDetailsSalon, post , deleteService } from "../../services/api";
import { withRouter } from "react-router-dom";

const titles = [
  {
    label: "نام",
  },
  {
    label: "توضیحات",
  },
  {
    label: "قیمت سرویس",
  },
  {
    label: "تخفیف سایت",
  },
  {
    label: "عملیات",
  },
];

const ServicesInfo = (props) => {
  const [services, setServices] = useState(null);

  useEffect(() => {
    getDetailsSalon();
}, []);

  const getDetailsSalon = () => {
    try {
      get("/services","classic", false)
      .then(function (res) {
        setServices(res.data.data);
      })
    } catch(err) {
        console.log("err", err);
    }
  }


  const handleService = (service) => {
    if (service) {
      props.history.push("/panel/services/editservice", { service });
    } else {
      props.history.push("/panel/services/addservice");
    }
  };

  function removeService(id) {
    try {
      let services_temp;
      let fields = {};
      fields.id = id;
      let res = post("/services/remove", fields, true)
          services_temp = services.filter((service) => {
            if (service.id !== id) {
              return service;
            }
          });
          setServices(services_temp);
    } catch(err) {
        console.log("err", err);
      };
  }

  return (
    <div className="col-lg-12 border-box my-5">
      <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
        <div className="col-lg-2 title-box border-box position-absolute d-flex justify-content-center">
          <i className="icon icon-creditCard text-dark icon-64 icon-pass"></i>
        </div>
      </div>
      <div className="row mt-2">
        <div className="col-lg-12 pb-5">
          <h5 className="text-center title-pass">سرویس ها</h5>
        </div>
      </div>
      <div className="col-lg-12 mr-4">
        <div className="row">
          <div className="col-11 mb-4">
            <button
              className="btn btn-primary"
              type="submit"
              onClick={() => handleService(null)}
            >
                ایجاد سرویس
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-11 text-center table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  {titles
                    ? titles.map((col) => {
                        return <th scope="col">{col.label}</th>;
                      })
                    : null}
                </tr>
              </thead>
              <tbody>
                {services
                  ? services.map((service, k) => {
                      
                      return (
                        <tr key={k} className="">
                          <td>{service.name}</td>
                          <td>{service.context}</td>
                          <td>{service.price}</td>
                          <td>{service.discount}</td>
                          <td>
                            <div className="">
                              <button
                                className="btn btn-danger btn-sm ml-2"
                                onClick={() => removeService(service.id)}
                              >
                                حذف
                              </button>
                              <button
                                className="btn btn-warning text-light btn-sm"
                                onClick={() => handleService(service)}
                              >
                                ویرایش
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })
                  : null}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(ServicesInfo);
