import React, { Component } from "react";
import { get , post , getFactors, accept_factor, edit_date_factor, delete_factor } from "../../services/api";
import Swal from "sweetalert2";
import moment from "moment-jalaali";
import EditFactor from "./DetailsFactor/EditFactor";

const titles = [
  {
    label: "نام سالن"
  },
  {
    label: "خدمات"
  },
  {
    label: "قیمت اصلی"
  },
  {
    label: "قیمت نهایی"
  },
  {
    label: "تخفیف سرویس"
  },
  {
    label: "تخفیف سایت"
  },
  {
    label: "تاریخ"
  },
  {
    label: "عملیات"
  }
];

const user = JSON.parse(localStorage.getItem("user"));
class AwaitingFactors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typeFactor: "awaiting_factors",
      showDatePicker: false,
    };
    this.getFactors();
  }

  
  getFactors = async () => {
    try {
        let res = await get(`/factors/${this.state.typeFactor}`, {}, true);
          this.setState({
            factors: res.data.data
          });
    } catch (err) {
        console.log("err", err);
    };
  }

  awaiting_factors = (factor, user) => {
    return (
      <div className="d-flex">
        <button className="btn btn-danger ml-1 btn-sm" onClick={() => this.DeleteFactor(factor.id , user.api_token)}>حذف</button>
        {this.state.typeFactor === "awaiting_factors" && user.level === "shop" && factor.accept_shop === 0 ? 
          <>
            <button className="btn btn-warning text-white ml-1 btn-sm" onClick={() => this.editFactor(factor.id)}>
              ویرایش
            </button>
            {this.state.showDatePicker ?
              <EditFactor user={user} factor={factor} editDate_factor={this.editDate_factor} />
            : 
            "" }                   
          </>
        : null}
        {(user.level === "shop" && factor.accept_shop === 0) || (user.level === "customer" && factor.accept_shop !== 0 && factor.type_cash === 0) ? (
          <button className="btn btn-success btn-sm" onClick={() => this.AcceptFactor(factor.id)}>
            {user.level === "shop" && factor.accept_shop === 0 ? "تایید" : "پرداخت"}
          </button>
        ) : null}
      </div>
    );
  };

  editFactor = id => {
    this.factor_id = id;
    this.setState({
      showDatePicker: true
    });
  };

  editDate_factor = (factor_id, dateTimePicker, api_token , jDate) => {
    this.setState({
      jDate,
      factor_id
    });
    const dateTime = moment(dateTimePicker).format("YYYY-MM-DD HH:mm:ss").toEnglish();
    const inf_date = { factor_id, dateTime, api_token };
    post('/factors/awaiting_factors/edit_date', inf_date, false)
      .then(
        function(res) {
          const factors = this.state.factors;
            factors.filter(factor => {
            if (factor.id === factor_id) {
             return {
                  ...factor,
                  ...factor.date = dateTimePicker,
                  ...factor.accept_shop = 1
                }
            } else return factor;
          });
          this.setState({
            result: res.data.data,
          });
          Swal.fire({
            title: "",
            text: `${this.state.result.message}`,
            icon: "success",
            timer: 2500,
            confirmButtonText: "تایید"
          });
        }.bind(this)
      )
      .catch(err => {
        console.log("err", err);
      });
    return dateTime;
  };

  DeleteFactor = async (factor_id , api_token) => {
    try {
      const inf_del = {
        "id" : factor_id ,
        "api_token" : api_token,
      }
      let res =await post("/factors/awaiting_factors/remove", inf_del, false);
          this.setState({
            status : res.status,
            result: res.data.data
          });
    } catch (err) {
      console.log("err", err);
    };
  }

  AcceptFactor =async (factor_id) => {
    try {
      let res =await post('/factors/awaiting_factors/accept', {factor_id} , true)
        this.setState({
          result: res.data
        });
    } catch (err) {
        console.log("err", err);
    };
  };

  render() {
    const { factors, typeFactor, showDatePicker, jDate, factor_id } = this.state;
    return (
      <div className="col-lg-12 border-box my-5">
        <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
            <div className="col-lg-2 col-md-2 col-sm-3 col-4 title-box border-box position-absolute d-flex justify-content-center">
                <i className="icon icon-creditCard text-dark icon-64 icon-pass"></i>
            </div>
        </div>
        <div className="row mt-2">
            <div className="col-lg-12 pb-5">
                <h5 className="text-center title-pass">سفارش های در انتظار</h5>
            </div>
        </div>

        <div className="row">
          <div className="col-lg-12 text-center font-size-13">
            <table class="table table-responsive table-bordered">
              <thead>
                <tr>
                  {titles ?
                  titles.map((col) => {
                    return(
                      <th scope="col">{col.label}</th>
                    )
                  })
                :  
                ""
                }
                </tr>
              </thead>
              <tbody>
                {
                  factors ? 
                    factors.map((factor , k) => {
                      const dateTime = moment(factor.date).format("jYYYY-jMM-jDD HH:mm:ss");
                      return(
                        <tr key={k} className="" >
                          <td>{factor.shop_id}</td>
                          <th scope="row">{factor.services}</th>
                          <td>{factor.price_primary}</td>
                          <td>{factor.price_final}</td>
                          <td>{factor.service_discount}</td>
                          <td>{factor.site_discount}</td>
                          <td>
                            { showDatePicker && jDate && factor.id === factor_id ? jDate : dateTime }
                          </td>
                          <td>
                            {typeFactor === "awaiting_factors" ? this.awaiting_factors(factor, user) : null}
                          </td>
                      </tr>
                      ) 
                    })
                  : 
                  ""
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
export default AwaitingFactors;