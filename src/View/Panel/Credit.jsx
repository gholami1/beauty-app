import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

const SignupSchema = Yup.object().shape({
  increaseCredit: Yup.string().required("Required"),
});

const Credit = (props) => {
  const [credit, setCredit] = useState(null);
  const [favoritesCredit, setFavoritesCredit] = useState(null);

  useEffect(() => {
    setCredit(JSON.parse(localStorage.getItem("user")).credit);
  }, []);

  const handlePrice = (e) => {
    if (e.target.value === "credit") {
      setFavoritesCredit(true);
    } else {
      setFavoritesCredit(false);
    }
  };

  const handleCredit = (values) => {
    let api_token = JSON.parse(localStorage.getItem("api_token"));
    return (
      <div className="col-lg-12 text-center px-0">
        <Formik
          enableReinitialize
          validateOnBlur={false}
          validateOnChange={false}
          initialValues={{
            increaseCredit: 10000,
            price: "",
          }}
          validationSchema={SignupSchema}
          onSubmit={(values) => {
            if (values.increaseCredit === "credit") {
              window.open(
                `http://localhost:8000/api/payment/add_credit?price=${values.price}&api_token=${api_token}`
              );
            } else {
              window.open(
                `http://localhost:8000/api/payment/add_credit?price=${values.increaseCredit}&api_token=${api_token}`
              );
            }
          }}
        >
          {({ errors }) => (
            <Form>
              <Field component="div" name="increaseCredit" className="mb-4 mr-3">
                <div className="custom-control custom-radio custom-control-inline">
                  <input
                    type="radio"
                    id="customRadioInline1"
                    name="increaseCredit"
                    className="custom-control-input"
                    defaultChecked
                    value={10000}
                    onChange={handlePrice}
                  />
                  <label
                    className="custom-credit custom-control-label mb-3"
                    for="customRadioInline1"
                  >
                    <span className=""> 10000 </span>
                    <span className=""> تومان </span>
                  </label>
                </div>
                <div className="custom-control custom-radio custom-control-inline">
                  <input
                    type="radio"
                    id="customRadioInline2"
                    name="increaseCredit"
                    className="custom-control-input"
                    value={20000}
                    onChange={handlePrice}
                  />
                  <label
                    className="custom-credit custom-control-label"
                    for="customRadioInline2"
                  >
                    <span className=""> 20000 </span>
                    <span className=""> تومان </span>
                  </label>
                </div>
                <div className="custom-control custom-radio custom-control-inline">
                  <input
                    type="radio"
                    id="customRadioInline3"
                    name="increaseCredit"
                    className="custom-control-input"
                    value={30000}
                    onChange={handlePrice}
                  />
                  <label
                    className="custom-credit custom-control-label"
                    for="customRadioInline3"
                  >
                    <span className=""> 30000 </span>
                    <span className=""> تومان </span>
                  </label>
                </div>
                <div className="custom-control custom-radio custom-control-inline">
                  <input
                    type="radio"
                    id="customRadioInline4"
                    name="increaseCredit"
                    className="custom-control-input"
                    value={40000}
                    onChange={handlePrice}
                  />
                  <label
                    className="custom-credit custom-control-label"
                    for="customRadioInline4"
                  >
                    <span className=""> 40000 </span>
                    <span className=""> تومان </span>
                  </label>
                </div>

                <div className="custom-control custom-radio custom-control-inline">
                  <input
                    type="radio"
                    id="customRadioInline5"
                    name="increaseCredit"
                    className="custom-control-input"
                    value={"credit"}
                    onChange={handlePrice}
                  />
                  <label
                    className="custom-credit custom-control-label"
                    for="customRadioInline5"
                  >
                    {" "}
                    مبلغ دلخواه{" "}
                  </label>
                </div>
              </Field>
              {errors.increaseCredit ? (
                <div>{errors.increaseCredit}</div>
              ) : null}

              {favoritesCredit ? (
                <div className="col-lg-6 col-11 my-2 m-auto">
                  <Field
                    className="input-form font-size-13 form-control text-center"
                    placeholder="لطفا مبلغ مورد نظر خود را وارد کنید."
                    name="price"
                    type="text"
                  />
                  {errors.price ? <div>{errors.price}</div> : null}
                </div>
              ) : (
                ""
              )}
              <div className="d-flex justify-content-center mt-4">
                <button className="btn btn-danger" type="submit">
                  پرداخت
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    );
  };

  return (
    <div className="border-box my-5">
      <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
        <div className="col-lg-2 col-md-2 col-sm-3 col-4 title-box border-box position-absolute d-flex justify-content-center">
          <i className="icon icon-email icon-64 icon-pass"></i>
        </div>
      </div>
      <div className="row mt-2">
        <div className="col-lg-12 pb-5">
          <h5 className="text-center title-pass">تغییر رمز عبور</h5>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12 pb-5 text-center">
          <h5 className="title-pass">اعتبار فعلی شما</h5>
          <h4 className="">
            <span className=""> {credit} </span>
            <span className="">تومان </span>
          </h4>
        </div>
      </div>
      <div className="row mr-lg-5 mb-5">{handleCredit()}</div>
    </div>
  );
};

export default withRouter(Credit);
