import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class FavLocation extends Component {
    constructor(props){
        super(props);
        this.state = {
            showFav : true,
        }
    }

    handleFav = () => {
        this.setState({
            showFav : false
        })
    }

    render() {
        const { showFav } = this.state;
        return (
            <div className="border-box my-5">
                <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
                    <div className="col-lg-2 col-4 col-md-2 col-sm-3 title-box border-box position-absolute d-flex justify-content-center">
                        <i className="icon icon-heart-red text-dark icon-64 icon-favorites"></i>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-lg-12 pb-5">
                        <h5 className="text-center title-pass">محل های محبوب من</h5>
                    </div>
                </div>
                <div className="d-flex justify-content-center my-3">
                    <div className="col-lg-2 col-4 text-center">
                        {showFav ? 
                            <i className="icon icon-heart-white icon-14 d-flex position-absolute fav-icon text-right" onClick={this.handleFav}></i>
                        : 
                            <i className="icon icon-heart icon-14 d-flex position-absolute fav-icon text-right"></i>
                        }
                        <Link to="" className="font-size-13">
                            <img className="img-fluid img-thumbnail img-fav" src="/img/1.jpg" />
                            <p className="mt-2">سینما پیتزا هزار و یک شب</p>
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default FavLocation;