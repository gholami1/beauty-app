import React from 'react';
import { post } from '../../services/api';
import { Formik, Form, Field } from 'formik';
import Swal from "sweetalert2";
import * as Yup from 'yup';


const SignupSchema = Yup.object().shape({
    prev_password : Yup.string()
    .min(6, 'حداقل طول رمز عبور قدیم 6 کاراکتر می باشد')
    .required('پر کردن فیلد رمز عبور قدیم اجباری می باشد'),
    password : Yup.string()
    .min(6, 'حداقل طول رمز عبور جدید 6 کاراکتر می باشد')
    .required('پر کردن فیلد رمز عبور جدید اجباری می باشد'),
    password_confirmation : Yup.string()
    .min(6, 'حداقل طول رمز عبور 6 کاراکتر می باشد')
    .required('پر کردن فیلد تکرار رمز عبور اجباری می باشد'),
  });


const ChangePassword = () => {

    const handleRes = (res) => {
        Swal.fire({
            title: "",
            text: `${res.message}`,
            icon: "success",
            timer: 2500,
            confirmButtonText: "تایید",
          });
    }

    const ChangePassword = () => (
        <div className="col-lg-12">
            <Formik
            enableReinitialize
            validateOnBlur={false}
            validateOnChange={false}
            initialValues={{
                prev_password : '',
                password : '',
                password_confirmation : '',
            }}
            validationSchema={SignupSchema}
            onSubmit={async (values) => {
                try {
                    let res = await post("/update_password" , values , true);
                    handleRes(res.data.data);
                } catch(err) {
                    console.log("err", err);
                };
            }}
            >
            {({ errors }) => (
                <Form>
                    <div className="col-lg-12 my-2 m-auto">
                        <Field className="input-form font-size-13 form-control text-center" placeholder="رمز عبور فعلی" name="prev_password" autoComplete="off" type="password"/>
                        {errors.prev_password ? <div>{errors.prev_password}</div> : null}
                    </div>
                    <div className="col-lg-12 my-2 mx-auto">
                        <Field className="input-form font-size-13 form-control text-center" placeholder="رمز عبور جدید" name="password" autoComplete="off" type="password" />
                        {errors.password ? <div>{errors.password}</div> : null}
                    </div>
                    <div className="col-lg-12 my-2 mx-auto">
                        <Field className="input-form font-size-13 form-control text-center" name="password_confirmation" placeholder="تکرار رمز عبور جدید" autoComplete="off" type="password" />
                        {errors.password_confirmation ? <div>{errors.password_confirmation}</div> : null}
                    </div>
                    <div className="d-flex justify-content-center mt-4">
                        <button className="btn btn-danger px-5 font-size-14" type="submit">ثبت تغییرات</button>
                    </div>
                </Form>
            )}
            </Formik>
        </div>
        )
   
    return (
        <div className="d-flex justify-content-center border-box mt-5 pb-5 position-relative">
            <div className="col-lg-2 col-5 col-sm-3 col-md-2 title-box-pass border-box position-absolute d-flex justify-content-center">
                <i className="icon icon-email icon-64 icon-pass"></i>
            </div>  
            <div className="row pt-5 mt-5">
                <div className="col-lg-12 pb-5">
                    <h5 className="text-center title-pass">تغییر رمز عبور</h5>
                </div>
                { ChangePassword() }
            </div>
        </div>
    );
}

export default ChangePassword;