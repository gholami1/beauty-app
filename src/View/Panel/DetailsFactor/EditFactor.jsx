import React, { Component } from 'react';
import ReactDatepicker from "../../../Components/ReactDatepicker";
import jMoment from "moment-jalaali";
import moment from "moment-jalaali";

class EditFactor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDate: jMoment()
            // value: momentJalaali(),
        }
    }
    
    handleDateChange = date => {
        const jDate = date.format("jYYYY-jMM-jDD HH:mm:ss");
        this.setState(
          {
            selectedDate: date,
            jDate
          }
          , () => this.props.editDate_factor(this.props.factor.id,this.state.selectedDate,this.props.user.api_token, this.state.jDate));
      };

    render() {
        const { selectedDate } = this.state;
        const { user , factor } = this.props;
        console.log(user , factor , selectedDate , 'factoruser');
        return (
            <div className="col-lg-3 px-0 position-input" id="login-signup">
                 <ReactDatepicker
                    id="data"
                    value={selectedDate}
                    onChange={this.handleDateChange}
                    className="calendar-Container size text-center position-absolute rounded search-date pl-2 form-control"
                />
            </div>
        );
    }
}

export default EditFactor;