import React, { useState, useEffect } from 'react';
import CardMessage from '../../Components/CardMessage'
import { get, post } from "../../services/api";

const Messages = () => {
    const [notifications, setNotifications] = useState(null);

    useEffect(() => {
        getNotificationRequest();
    }, []);

    const getNotificationRequest = async () => {
        try {
            let response = await get('/notification', {}, true);
            console.log(response, 'response');
            setNotifications(response.data.data);
        } catch (error) {
        }
    }

    const read = async (id) => {
        try {
            await post('/notification/read_notif', { id }, true);
            getNotificationRequest();
        } catch (err) {
            console.log(err)
        }
    }

    const remove = async (id) => {
        await post('/notification/del_notif', { id }, true);
        getNotificationRequest();
    }

    return (
        <div className="border-box my-5 background-gray" >
            <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
                <div className="col-lg-2 col-4 col-md-2 col-sm-3 title-box border-box position-absolute d-flex justify-content-center">
                    <i className="icon icon-email text-dark icon-64 icon-pass"></i>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-lg-12 pb-5 pt-3">
                    <h5 className="text-center title-pass">پیام ها</h5>
                </div>
            </div>
            {
                notifications ? notifications.map(message =>
                    <CardMessage message={message} read={read} remove={remove} />
                )
                    :
                    null
            }
        </div>
    );
}

export default Messages;