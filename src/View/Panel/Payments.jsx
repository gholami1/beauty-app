import React, { useEffect } from 'react';

const Payments = () => {

    useEffect(() => {

    },[]);

    return (
        <div className="border-box my-5">
            <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
                <div className="col-lg-2 col-4 col-md-2 col-sm-3 title-box border-box position-absolute d-flex justify-content-center">
                    <i className="icon icon-creditCard text-dark icon-64 icon-pass"></i>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-lg-12 pb-5 pt-3">
                    <h5 className="text-center title-pass">پرداختی های من</h5>
                </div>
            </div>
            <div className="d-flex justify-content-center my-3">
                <div className="col-lg-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">First</th>
                                <th scope="col">Last</th>
                                <th scope="col">Handle</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="d-flex justify-content-center my-3">
                
            </div>
        </div>
    );
}

export default Payments;