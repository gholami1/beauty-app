import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import Swal from "sweetalert2";
import { getFactors, post , createServices } from "../../services/api";

const SignupSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  context: Yup.string().required("Required"),
  price: Yup.string().required("Required"),
  discount: Yup.string().required("Required"),
});

const AddService = (props) => {
  const [service, setService] = useState(null);

  useEffect(() => {
    if (props.location.pathname === "/panel/services/editservice") {
      setService(props.location.state.service);
    }
  }, []);

  const handleEdit = (result) => {
    Swal.fire({
      title: "",
      text: `${result.message}`,
      icon: "success",
      timer: 2500,
      confirmButtonText: "تایید",
    });
    props.history.push("/panel/servicesinfo");
  };


  const handleServices = () => {
    return (
      <div className="col-lg-6">
        <Formik
          enableReinitialize
          validateOnBlur={false}
          validateOnChange={false}
          initialValues={{
            name: service ? service.name : "",
            price: service ? service.price : "",
            discount: service ? service.discount : "",
            context: service ? service.context : "",
          }}
          validationSchema={SignupSchema}
          onSubmit={async (values) => {
            if (service) {
              try {
                values.id = service.id;
                let res = post("services/edit", values, true);
                handleEdit(res.data.data, values);
              } catch (err) {
                console.log("err", err);
              };
            } else {
              try {
                let res = post("services/create", values, true);
                handleEdit(res.data.data);
              } catch(err) {
                  console.log("err", err);
              };
            }
          }}
        >
          {({ errors }) => (
            <Form>
              <div className="col-lg-12 my-2 m-auto">
                <Field
                  className="input-form font-size-13 form-control text-center"
                  placeholder="نام سرویس"
                  name="name"
                  type="text"
                />
                {errors.name ? <div>{errors.name}</div> : null}
              </div>
              <div className="col-lg-12 my-2 mx-auto">
                <Field
                  className="input-form font-size-13 form-control text-center"
                  placeholder="توضیحات"
                  name="context"
                  type="text"
                />
                {errors.context ? <div>{errors.context}</div> : null}
              </div>
              <div className="col-lg-12 my-2 mx-auto">
                <Field
                  className="input-form font-size-13 form-control text-center"
                  name="price"
                  placeholder="قیمت"
                  type="text"
                />
                {errors.price ? <div>{errors.price}</div> : null}
              </div>
              <div className="col-lg-12 my-2 mx-auto">
                <Field
                  className="input-form font-size-13 form-control text-center"
                  name="discount"
                  placeholder="تخفیف"
                  type="text"
                />
                {errors.discount ? <div>{errors.discount}</div> : null}
              </div>
              <div className="d-flex justify-content-center mt-4">
                <button
                  className={
                    service
                      ? "btn btn-success px-5 font-size-14"
                      : "btn btn-danger px-5 font-size-14"
                  }
                  type="submit"
                >
                  {service ? "ویرایش سرویس" : "ایجاد سرویس"}
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    );
  };

  return (
    <div className="row">
      <div className="col-11 text-center">
        <p className="font-weight-500 mb-5 mr-4">
          {service ? "ویرایش سرویس" : "ایجاد سرویس"}
        </p>
      </div>
      <div className="d-flex justify-content-center col-11">
        {handleServices()}
      </div>
    </div>
  );
};

export default withRouter(AddService);
