import React, { Component, Fragment } from 'react';
import { TextInput, View, Text, StyleSheet } from 'react-native';
import * as yup from 'yup';
import { Formik } from 'formik';
import { Container, Header, Content, Left, Body, Right } from 'native-base';
import { post } from '../utils/api';
import { getHeight as hp, getWidth as wp } from '../utils/dimension';
import { form, index, button } from './../assets/styles';
import { Button } from '../components/common';

export class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: 'form_password',
        }
    }

    form_password() {
        if (this.state.expanded === 'form_password') {
            return (
                <Formik
                    style={form.StyleForm}
                    initialValues={{
                        prev_password: '', password: '', password_confirmation: ''
                    }}
                    onSubmit={(value) => this.UpdatePassword(value)}
                    validationSchema={yup.object().shape({
                        prev_password: yup
                            .string()
                            .min(6, 'حداقل طول رمز عبور قدیم 6 کاراکتر می باشد')
                            .required('پر کردن فیلد رمز عبور قدیم اجباری می باشد'),
                        password: yup
                            .string()
                            .min(6, 'حداقل طول رمز عبور جدید 6 کاراکتر می باشد')
                            .required('پر کردن فیلد رمز عبور جدید اجباری می باشد'),
                        password_confirmation: yup
                            .string()
                            .min(6, 'حداقل طول رمز عبور 6 کاراکتر می باشد')
                            .required('پر کردن فیلد تکرار رمز عبور اجباری می باشد'),
                    })}
                >
                    {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
                        <Fragment>
                            <TextInput
                                style={form.item}
                                value={values.prev_password}
                                onChangeText={handleChange('prev_password')}
                                placeholder="رمز عبور قدیم"
                                onBlur={() => setFieldTouched('paprev_passwordssword')}
                                secureTextEntry={true}
                            />
                            <TextInput
                                style={form.item}
                                value={values.password}
                                onChangeText={handleChange('password')}
                                placeholder="رمز عبور جدید"
                                onBlur={() => setFieldTouched('password')}
                                secureTextEntry={true}
                            />
                            <TextInput
                                style={form.item}
                                value={values.password_confirmation}
                                onChangeText={handleChange('password_confirmation')}
                                placeholder="تکرار رمز عبور جدید"
                                onBlur={() => setFieldTouched('password_confirmation')}
                                secureTextEntry={true}
                            />
                            <View style={index.center}>
                                <Button
                                    style={button.button_approve} textStyle={button.approve_textStyle}
                                    whenPress={errors.prev_password || errors.password || errors.password_confirmation ?
                                        notif(true,'err',errors[Object.keys(errors)[0]])
                                        : handleSubmit}>
                                    ثبت
                                </Button>
                            </View>
                        </Fragment>
                    )}
                </Formik>
            );
        }
    }

    render() {
        return (
            <Container>
                {/* <CheckLogin navigation={this.props.navigation} /> */}
                <Header style={{ backgroundColor: '#192a56' }} androidStatusBarColor='#192a56' iosBarStyle='light-content'>
                    <Left>
                    </Left>
                    <Body>
                    </Body>
                    <Right>
                        <Text style={{ fontFamily: 'IRANSansMobile', color: 'white', marginRight: 10 }}>تنظیمات</Text>
                    </Right>
                </Header>
                <Content>
                    <Button style={styles.touchableButton} textStyle={[index.font_white_17, { textAlign: 'center' }]}
                        whenPress={() => { this.setState({ expanded: 'form_password' }); }}>
                        تغییر رمز عبور
                </Button>
                    {this.form_password()}
                </Content>
            </Container>
        )
    }
    async UpdatePassword(fields) {
        try {
            await post('/update_password', fields, true);
            notif(true);
        } catch (error) {
            console.log(error);
        }
    }
}


const styles = StyleSheet.create(
    {
        modal: {
            flex: 1,
        },
        touchableButton: {
            flexDirection: 'row',
            justifyContent: 'center',
            borderRadius: 10,
            width: wp(98),
            height: hp(9),
            padding: hp(2),
            backgroundColor: '#3166E2',
            margin: '1%',
        },
        touchableButton_small: {
            borderRadius: 25,
            width: '25%',
            padding: 10,
            backgroundColor: '#3166E2',
            marginTop: '1%',
            marginBottom: '1%',
        },
    });
