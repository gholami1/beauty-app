import React, { Component } from "react";
import Select from "react-select";
import ReactModal from "react-modal";
import MapBox from "../../Components/MapBox";
import { get, getCity, upload } from "../../services/api";
import Swal from "sweetalert2";

let provinces = [];
class SalonInfo extends Component {
  constructor(props) {
    super(props);
    this.cities = [];
    this.state = {
      showModal: false,
    };
  }

  componentDidMount() {
    this.getShop();
  }
   getShop = async () => {
    try{
      let res = await get('/shop/getShop', {} ,true);
        this.setState({
          id: res.data.id,
          gender: res.data.sex,
          name: res.data.name,
          street: res.data.street,
          address: res.data.address,
          images: res.data.images,
          context: res.data.context,
          lat: res.data.lat,
          lng: res.data.lng,
          city_id: res.data.city,
          province_id: res.data.province,
        }, () => this.get_city());
    } catch (err) {
      console.log(err,'err_async')
    }
  }
  get_city=()=>{
    getCity()
    .then(
      function (res) {
        this.setState(
          {
            province: res.data[0].data,
            city: res.data[1].data,
          },
          () => this.handleCity()
        );
      }.bind(this)
    )
    .catch((err) => {
      console.log("err", err);
    });
  }

  handleCity = () => {
    let city_name = this.state.city.filter(c => {
      if(c.id === this.state.city_id) {
        this.state.province.filter(p => {
          if(p.id === c.province) {
            this.setState({
              selectedProvice: {
                label: p.name,
                value: p.id,
              },
              selectedCity: {
                label: c.name,
                value: c.id,
              },
            })
          }
        })
      }
    });
  };

  handleCloseModal = () => {
    this.setState({
      showModal: false,
    });
  };

  handleChangePhoto = (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    let url = reader.readAsDataURL(file);
    reader.onloadend = (e) => {
      this.setState({
        file,
        imgSrc : [reader.result]
      } , () => {console.log(this.state.file , this.state.imgSrc , 'imgSrc')})
    }
  }

  handleChangeSalon = (e) => {
    const target = e.target;
    this.setState({
      [target.name]: target.value,
    }, () => {console.log(this.state.gender , 'vaaal')});
  };

  handleChangeAddress = () => {
    this.setState({
      showModal: true,
    });
  };

  selectChangeProvice = (selectedProvice) => {
      this.cities=[];
      this.state.city.filter((c, k) => {
        return c.province === selectedProvice.value
          ? 
          this.cities.push({ key: k, value: c.id, label: c.name })
          : "";
      });
      this.setState({
      selectedProvice,
      selectedCity : {
        label : this.cities[0].label,
        value : this.cities[0].value,
      },
    });
  };

  selectChangeCity = (selectedCity) => {
    this.setState({
      selectedCity,
    });
  };

  handleLatlong = (lat, lng) => {
    this.setState({
      lat, lng
    });
  };
 
  handleChangeProfile = async () => {
    try {
        let data = new FormData();
      data.append('id',this.state.id);
      data.append('sex', this.state.gender)
      data.append('name', this.state.name);
      data.append('street', this.state.street);
      data.append('address', this.state.address);
      data.append('context', this.state.context);
      data.append('lat', this.state.lat);
      data.append('lng', this.state.lng);
      data.append('city', this.state.selectedCity.value);
      this.state.file ? data.append('image', this.state.file) : data.append('image', null);
      let res =  await upload("/shop/update", data ,true)
        Swal.fire({
          title: "",
          text: `${res.data.data.message}`,
          icon: "success",
          timer: 2500,
          confirmButtonText: "تایید",
        });
      }catch (err) {
          console.log('err', err);
      }; 
  }


  render() {
    const { name, street, address, context, showModal, images, province, lat, lng, gender, selectedProvice, selectedCity } = this.state;
    if (province) {
      province.forEach((p, k) => {
        provinces.push({ key: k, value: p.id, label: p.name });
      });
    }
  
    return (
      <div className="border-box my-5">

        <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
          <div className="col-lg-2 col-4 col-md-2 col-sm-3 title-box border-box position-absolute d-flex justify-content-center">
            <i className="icon icon-creditCard text-dark icon-64 icon-pass"></i>
          </div>
        </div>
        <div className="row mt-2">
          <div className="col-lg-12 pb-5">
            <h5 className="text-center title-pass">ویرایش اطلاعات سالن</h5>
          </div>
        </div>
        <div className="col-12 text-right">
          <p className="font-weight-500 mb-4 mr-4">اطلاعات سالن</p>
            
          <div className="d-flex mr-1 pr-4 pb-3">
            <div className="custom-control custom-radio">
                <input id="female" name="gender" className="flight-class custom-control-input" onChange={this.handleChangeSalon} required type="radio" value={"1"} checked={this.state.gender === 1 ? true : false} />
                <label className="custom-control-label text-dark" htmlFor="female">زن</label>
            </div>
            <div className="custom-control custom-radio mr-1">
                <input id="men" name="gender" className="flight-class custom-control-input" onChange={this.handleChangeSalon} required type="radio" value={"0"} checked={this.state.gender === 0 ? true : false} />
                <label className="custom-control-label text-dark" htmlFor="men">مرد </label>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4 col-6 form-group position-relative">
              <i className="icon icon-18 icon-user position-absolute position"></i>
              <input className="form-control pr-5" name="name" id="name" placeholder=" نام سالن" type="text" value={name} onChange={this.handleChangeSalon} />
            </div>
            <div className="col-lg-4 col-6 form-group position-relative">
              <i className="icon icon-18 icon-user position-absolute position"></i>
              <input
                className="form-control pr-5" name="street" id="street" placeholder="خیابان" type="text" value={street} onChange={this.handleChangeSalon} />
            </div>
            <div className="col-lg-4 col-10 mx-auto form-group position-relative">
              <i className="icon icon-18 icon-mobile position-absolute position"></i>
              <input className="form-control pr-5" name="address" id="address" placeholder="آدرس سالن" type="text" value={address} onChange={this.handleChangeSalon} />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-right">
            <p className="font-weight-500 text-center my-5 mb-2">عکس سالن</p>
            <div className="d-flex col-12">
              <div className="col-12 form-group d-flex justify-content-center h-210 position-relative">
                  <i className="icon icon-48 icon-positive position-icon position-absolute"></i>
                  <input type="file" name="file" onChange={this.handleChangePhoto} className="img-inupt position-absolute"/>
                  <img className="col-8 img-thumbnail" src={this.state.imgSrc ? this.state.imgSrc : `http://localhost:8000${images}`} />
              </div>
            </div>
          </div>
        </div>
       
        <div className="row">
          <div className="col-12 d-flex justify-content-center pt-5 form-group position-relative">
            <textarea className="form-control col-11" rows="7" col="5" value={context} name="context" id="context" onChange={this.handleChangeSalon} ></textarea>
          </div>
        </div>
        
        <div className="row">
          <div className="col-12 text-right">
            <p className="font-weight-500 mb-2 mr-4">اطلاعات سالن</p>
            <div className="d-flex justify-content-between mt-3">
              <div className="col-6 form-group position-relative">
                  <Select
                    className="citySearchAutoCompeletSelect"
                    value={selectedProvice}
                    onChange={this.selectChangeProvice}
                    options={provinces}
                    placeholder={<span><i className="icon icon-location icon-14 mb-1"></i>استان</span>} 
                  />
              </div>
              <div className="col-6 form-group position-relative">
                  <Select
                    className="citySearchAutoCompeletSelect"
                    value={selectedCity}
                    onChange={this.selectChangeCity}
                    options={this.cities}
                    placeholder={<span><i className="icon icon-location icon-14 mb-1"></i>شهر</span>}
                  />
              </div>
            </div>
          </div>
        </div>

        <div className="d-flex justify-content-center pt-3 btn-sm col-12">
          <button className="btn btn-outline-danger px-5 py-2" type="submit" onClick={this.handleChangeAddress}>ثبت آدرس جدید</button>
        </div>

        <div className="d-flex justify-content-center pt-3 btn-sm col-12">
          <button className="btn btn-danger px-5 py-2" type="submit" onClick={this.handleChangeProfile}> ثبت تغییرات</button>
        </div>
        <ReactModal
          className="ReactModal__Content"
          role="dialog"
          blockScroll={false}
          isOpen={showModal}
          onRequestClose={this.handleCloseModal}
          shouldCloseOnOverlayClick={true}
          style={{
            content: {
              overlfow: "scroll",
              height: "100vh",
            },
            overlay: {
              backgroundColor: "rgb(63,63,63,0.75)",
              zIndex: 10009,
            },
          }}
        >
          <MapBox
            onClose={this.handleCloseModal}
            Latitude={lat}
            Longitude={lng}
            zoom={12}
            latlong={this.handleLatlong}
          />
        </ReactModal>
      </div>
    );
  }
}

export default SalonInfo;
