import React, { useEffect, useState } from 'react';
import { getCity , profile , post } from '../../services/api';
import Select from 'react-select';
import ReactModal from "../../Components/ReactModal";
import MapBox from "../../Components/MapBox";
import Swal from "sweetalert2";

const UserInfo = (props) =>  {
    const [showModal, setShowModal] = useState(false)
    const [info, setInfo] = useState({})


    useEffect(() => {
        const UserInfo = JSON.parse(localStorage.getItem('user'));
        setInfo({
            api_token: UserInfo.api_token,
            name: UserInfo.name,
            family: UserInfo.last_name,
            mobile: UserInfo.mobile,
            phone: UserInfo.mobile,
            email: UserInfo.email,
            address: UserInfo.address,
            national_code: UserInfo.national_code,
            lat: UserInfo.lat,
            lng: UserInfo.lng,
        })

        getCity()
        .then(function (res) {
            setInfo({
                province: res.data[0].data,
                city: res.data[1].data,
            }, () => handleCity(this.state.city , UserInfo , this.state.province));
        }.bind(this))
        .catch(err => {
            console.log('err', err);
        }); 
    },[]);


    const handleCity = (city , UserInfo , province) => {
        city.forEach((c , k) => {
            if(c.id === UserInfo.city) {
                province.forEach((p , k) => {
                    if(p.id === c.province) {
                        this.setInfo({
                            selectedProvice : {
                                label : p.name,
                                value : p.id
                            },
                            selectedCity : {
                                label : c.name,
                                value : c.id,
                            },
                        })
                    }
                })
            }
        })
    }
    
    const handleChange = (e) => {
        const target = e.target;
        setInfo({
            name : target.value,
        })
    }

    const selectChangeProvice = (selectedProvice) => {
        setInfo({selectedProvice})
    }

    const selectChangeCity = (selectedCity) => {
        setInfo({selectedCity})
    }

    const handleLatlong = (lat , lng) => {
        setInfo({
            lat,
            lng
        })
    }

    const handleChangeProfile = async () => {
        try {
            const setProfile = {
                "api_token": this.state.api_token,
                "name": this.state.name,
                "last_name": this.state.family,
                "mobile": this.state.mobile,
                "email": this.state.email,
                "city": this.state.selectedCity ? this.state.selectedCity.value : '',
                "address": this.state.address,
                "lat": this.state.lat,
                "lng": this.state.lng,  
            };
    
            let res = await post('/update_user' , setProfile , false)
            // .then(function (res) {
                sessionStorage.setItem('user',JSON.stringify(setProfile));
                // this.setState({
                //     result: res.data.data
                // });
                Swal.fire({
                    title: "",
                    // text: `${result.message}`,
                    icon: "success",
                    timer: 2500,
                    confirmButtonText: "تایید",
                  });
            // }.bind(this))
        } catch (err) {
            console.log('err', err);
        };
    }

    const handleChangeAddress = () => {
        setShowModal(true);
    }

    const handleCloseModal = () => {
        setShowModal(false);
    }
   

        let cities=[] , provinces=[];
        if(info.province) {
            info.province.map((p , k) => {
                provinces.push({ key : k, value: p.id , label: p.name })
            }) 
        }
        
        if(info.city && info.selectedProvice) {
            info.city.filter((c , k) => {
                return info.selectedProvice.value === c.province ?
                    cities.push({ key : k, value: c.id , label: c.name })
                : ''
            })
        }

        return (
              <div className="border-box my-5">
                <div className="d-flex justify-content-center mt-5 pb-5 position-relative">
                    <div className="col-5 col-lg-2 title-box border-box position-absolute d-flex justify-content-center">
                        <i className="icon icon-creditCard text-dark icon-64 icon-pass"></i>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-lg-12 pb-5">
                        <h5 className="text-center title-pass">اطلاعات کاربری</h5>
                    </div>
                </div>
                <div className="text-right">
                    <p className="font-weight-500 mb-2 mr-lg-4 mr-3">اطلاعات شما</p>
                    <div className="row mx-0 px-0 col-12">
                        <div className="col-6 col-lg-4 form-group position-relative">
                            <i className="icon icon-18 icon-user position-absolute position"></i>
                            <input className="form-control pr-5" name="name" id="name" placeholder="نام" type="text" value={info.name} onChange={handleChange} />
                        </div>
                        <div className="col-6 col-lg-4 form-group position-relative">
                            <i className="icon icon-18 icon-user position-absolute position"></i>
                            <input className="form-control pr-5" name="family" id="family" placeholder="نام خانوادگی" type="text" value={info.family} onChange={handleChange} />
                        </div>
                        <div className="col-12 col-sm-11 mx-auto col-lg-4 form-group position-relative">
                            <i className="icon icon-18 icon-mobile position-absolute position"></i>
                            <input className="form-control pr-5" name="mobile" id="mobile" placeholder="موبایل" type="text" value={info.mobile} />
                        </div>
                    </div>
                    <div className="d-flex justify-content-center">
                        <div className="col-lg-12 col-12 col-sm-11 mx-auto form-group position-relative">
                            <i className="icon icon-18 icon-mobile position-absolute position"></i>
                            <input className="form-control pr-5" name="email" id="email" placeholder="پست الکترونیکی" value={info.email} onChange={handleChange} />
                        </div>
                    </div>
                </div>
                <div className="col-12 text-right">
                        <p className="mr-lg-4 mr-2">آدرس</p>
                        <div className="row mx-lg-1">
                            <div className="col-lg-2 col-6 form-group">
                                <Select
                                    className="citySearchAutoCompeletSelect"
                                    value={info.selectedProvice}
                                    onChange={selectChangeProvice}
                                    options={provinces}
                                    placeholder={<span><i className="icon icon-location icon-14 mb-1"></i>استان</span>}
                                />
                            </div>
                            <div className="col-lg-2 col-6 form-group">
                                <Select
                                    className="citySearchAutoCompeletSelect"
                                    value={info.selectedCity}
                                    onChange={selectChangeCity}
                                    options={cities}
                                    placeholder={<span><i className="icon icon-location icon-14 mb-1"></i>شهر</span>}
                                />
                            </div>
                            <div className="col-lg-4 col-6 form-group position-relative">
                                <i className="icon icon-18 icon-email position-absolute position"></i>
                                <input className="form-control pr-5" name="phone" id="phone" placeholder="تلفن همراه" value={info.phone} onChange={handleChange} />
                            </div>
                            <div className="col-lg-4 col-6 form-group position-relative">
                                <i className="icon icon-18 icon-pin position-absolute position"></i>
                                <input className="form-control pr-5" name="address" id="address" placeholder="آدرس" value={info.address} onChange={handleChange} />
                            </div>
                        </div>
                    </div>
                    <div className="d-flex justify-content-center pt-3 btn-sm col-12">
                        <button className="btn btn-outline-danger px-5 py-2" type="submit" onClick={handleChangeAddress}>ثبت آدرس جدید</button>
                    </div>
                    <div className="d-flex justify-content-center pt-3 mb-3 btn-sm col-12">
                        <button className="btn btn-danger px-5 py-2" type="submit" onClick={handleChangeProfile}>ثبت تغییرات</button>
                    </div>
                    <ReactModal
                        className="ReactModal__Content"
                        role="dialog"
                        blockScroll={false}
                        isOpen={showModal}
                        onRequestClose={handleCloseModal}
                        shouldCloseOnOverlayClick={true}
                        style={{
                            content: {
                            overlfow: "scroll",
                            height: "100vh"
                            },
                            overlay: {
                            backgroundColor: "rgb(63,63,63,0.75)",
                            zIndex: 10009
                            }
                        }}
                    >
                        <MapBox onClose={handleCloseModal} Latitude={info.lat} Longitude={info.lng} zoom={12} latlong={handleLatlong} />
                    </ReactModal>
                    </div>
        );
}

export default UserInfo;