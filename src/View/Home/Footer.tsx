import React from 'react';

const Footer: React.FC = (props) => {
    
        return (
            <div className="container">
                <div className="row text-light text-2 ">
                    <div className="col-lg-2 text-right">
                        <p className="font-weight-bold">با ما در ارتباط باشید</p>
                        <div className="col-lg-10 border-top-light pb-3"></div>
                        <ul className="pr-0">
                            <li className="pb-1">تماس با ما</li>
                            <li className="pb-1">تیم ما</li>
                            <li className="pb-1">حریم خصوصی</li>
                            <li className="pb-1">ثبت شکایت</li>
                        </ul>
                    </div>
                    <div className="col-lg-2">
                        <p className="font-weight-bold">بیشتر بدانید</p>
                        <div className="col-lg-5 border-top-light pb-3"></div>
                        <ul className="pr-0">
                            <li className="pb-1">درباره ما</li>
                            <li className="pb-1">قوانین سایت</li>
                            <li className="pb-1">پرسش های متداول</li>
                            <li className="pb-1">اپلیکیشن موبایل</li>
                            <li className="">بسته های خدماتی ویژه</li>
                        </ul>
                    </div>
                    <div className="col-lg-4">
                        <p className="col-lg-2 pr-0 pb-3 border-bottom-light">وبلاگ</p>

                        <div className="row pt-1">
                            <div className="col-lg-4">
                                <img className="img-fluid rounded" src="/img/1.jpg" />
                            </div>
                            <div className="col-lg-8">
                                <p className="">
                                صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برند.
                                </p>
                                <div className="row">
                                    <div className="col-lg-4">
                                        <i className=""></i>
                                        <span>14 تیر 99</span>
                                    </div>
                                    <div className="col-lg-4">
                                        <i className=""></i>
                                        <span>ادمین</span>
                                    </div>
                                    <div className="col-lg-4">
                                        <i className=""></i>
                                        <span>19</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row pt-2">
                            <div className="col-lg-4">
                                <img className="img-fluid rounded" src="/img/1.jpg" />
                            </div>
                            <div className="col-lg-8">
                                <p className="">
                                صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برند.
                                </p>
                                <div className="row">
                                    <div className="col-lg-4">
                                        <i className=""></i>
                                        <span>14 تیر 99</span>
                                    </div>
                                    <div className="col-lg-4">
                                        <i className=""></i>
                                        <span>ادمین</span>
                                    </div>
                                    <div className="col-lg-4">
                                        <i className=""></i>
                                        <span>19</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 text-center">
                        <p className="font-weight-bold">عضویت در خبرنامه</p>
                        <div className="col-lg-5 border-top-light m-auto pb-3"></div>
                        <span className="">برای عضویت در خبرنامه و دریافت جدیدترین مطالب تخفیف‌‌ها و جشنواره‌ها ایمیل خود را وارد کنید</span>
                        <div className="d-flex my-4 bg-light">
                            <input className="form-control btn-join" placeholder="ایمیل" />
                            <div className="col-lg-2 p-2  bg-danger">عضویت</div>
                        </div>
                        <div className=" border-light">
                            <ul className="d-flex justify-content-center pr-0 pt-4 social-list">
                                <li className="social-box ml-3">
                                    <i className="icon icon-telegram icon-22 m-1"></i>
                                </li>
                                <li className="social-box ml-3">
                                    <i className="icon icon-instagram icon-20 m-1"></i>
                                </li>
                                <li className="social-box ml-3">
                                    <i className="icon icon-linkedin icon-20 m-1"></i>
                                </li>
                            </ul>
                        </div> 
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-lg-6 m-auto border-top-light text-light pt-3 text-center">
                        کپی
                    </div>
                </div>
            </div>
                
        );
}

export default Footer;