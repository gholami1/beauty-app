import React, { Component } from "react";
import Slider from "react-slick";
import '../../assets/css/slick.css';

interface setting {
    dots: boolean,
    infinite: boolean,
    slidesToShow: number,
    slidesToScroll: number,
    autoplay: boolean,
    autoplaySpeed: number,
    pauseOnHover: boolean
}

const HallFavorites : React.FC = (props) => {
 
    var settings : setting = {
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      pauseOnHover: true
    };
    return (
      <Slider {...settings}>
        <div>
          <h3>1</h3>
        </div>
        <div>
          <h3>2</h3>
        </div>
        <div>
          <h3>3</h3>
        </div>
        <div>
          <h3>4</h3>
        </div>
        <div>
          <h3>5</h3>
        </div>
        <div>
          <h3>6</h3>
        </div>
      </Slider>
    );
}
export default HallFavorites;