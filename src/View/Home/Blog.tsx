import React, { useEffect } from 'react';
import Img1 from "../../assets/img/1.jpg";

const Blog : React.FC = (props) => {

    useEffect(() => {

    },[]);

    return (
        <section className="ftco-section text-right">
            <div className="container">
                <div className="row justify-content-center mb-5 pb-3">
                    <div className="col-md-7 heading-section ftco-animate text-center fadeInUp ftco-animated">
                        <h2 className="mb-4">شامل حروف چینی دستاورد</h2>
                        <p>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                        </p>
                    </div>
                </div>
                <div className="row d-flex">
                    <div className="col-md-4 d-flex ftco-animate fadeInUp ftco-animated">
                        <div className="blog-entry align-self-stretch">
                            <a href="blog-single.html" className="block-20">
                                <img src={Img1} />
                            </a>
                            <div className="text pb-4 d-block">
                                <div className="meta">
                                <div><a href="#">مهر 1399</a></div>
                                <div><a href="#">ادمین</a></div>
                                <div><a href="#" className="meta-chat"><span className="icon-chat"></span> 3</a></div>
                                </div>
                                <h6 className="heading mt-2"><a href="#"> شامل حروف چینی دستاوردهای اصلی</a></h6>
                                <p> و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Blog;
