import React from 'react';
import Footer from './Footer';
import SliderBackground from '../../Components/SliderBackground';
import Search from '../Search/Search';
import ItemBeauty from './ItemBeauty';
import HallFavorites from './HallFavorites';
import Blog from './Blog';

const Home : React.FC = (props) => {
    
    return (
        <div id="main-wrapper">
            <div className="" id="">
                <div className="hero-bg-slideshow pb-5 owl-carousel">
                    <SliderBackground />
                    <div className="hero-content py-lg-5 my-lg-5">
                        <div className="container mt-5 pt-4">
                            <div id="mySearch">
                                <Search />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <ItemBeauty />
                </div>
                <h4 className="text-center mb-2"> برترین تخفیف ها</h4>
                <div className="slider-list">
                    <HallFavorites />
                </div>
                <div id="section-counter">
                    <div className="overlay"></div>
                </div>
                    <Blog />
                <div className="footer text-right p-5">
                    <Footer />
                </div>
            </div>  
        </div>
    );
}

export default Home;