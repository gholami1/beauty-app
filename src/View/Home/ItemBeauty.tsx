import React from 'react';

const ItemBeauty : React.FC  = (props) => {
    return (
        <>
        <div className="d-flex justify-content-between total-box-beauty text-center">
            <div className="col-lg-3 cursor pt-4">
                <div className="">
                    <i className="icon icon-skin-care icon-48"></i>
                </div>
                <div className="pt-2 text-secondary">
                    زیبایی و مراقبت از پوست
                </div>
            </div>
            <div className="col-lg-3 cursor pt-4">
                <div className="">
                    <i className="icon icon-makeup icon-48"></i>
                </div>
                <div className="text-secondary pt-2">
                خدمات آرایشی
                </div>
            </div>
            <div className="col-lg-3 cursor pt-4">
                <div>
                    <i className="icon icon-hair-style icon-48"></i>
                </div>
                <div className="text-secondary pt-2">
                مدل مو
                </div>
            </div>
            <div className="col-lg-3 cursor pt-4">
                <div>
                    <i className="icon icon-nail icon-48"></i>
                </div>
                <div className="text-secondary pt-2">
               خدمات ناخن
                </div>
            </div>
        </div>
    </>
    )
}

export default ItemBeauty;