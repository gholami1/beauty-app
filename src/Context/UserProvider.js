import React , { Component } from "react";

export const UserContext = React.createContext()

export default class UserProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "saaa",
            loggedIn: false,
        }
    }

    handleLogout = () => {
        this.setState({
            loggedIn: false,
            name: "",
        })
    }

    render() {
        return(
                <UserContext.Provider value={{ user: this.state, logout: this.handleLogout }}>
                    {this.props.children}
                </UserContext.Provider>
        )
    }
}
