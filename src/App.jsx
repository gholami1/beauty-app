import React, {Component} from 'react';
import { path } from './routes/path';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import 'sweetalert2/src/sweetalert2.scss';
import PrivateRoute from './routes/PrivateRoute';
import { connect } from 'react-redux';
import '../src/helperFunctions/convertNumber.js';
import {
  onMessageListener,
  getMessagingToken,
} from './helperFunctions/firebase-messaging';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { get } from './services/api';
import Main from './Layout/Main';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userType: null,
    };
  }

  componentWillUnmount() {
    this.unsubscribe();
    // this.onTokenRefresh();
  }

  componentDidMount() {
    this.get_notification();
    getMessagingToken();
    this.unsubscribe = onMessageListener(this.get_notification());

    const user =
      localStorage.getItem('user') !== undefined
        ? JSON.parse(localStorage.getItem('user'))
        : null;
    if (user) {
      this.setState({userType: user.level});
    }
  }
  async get_notification() {
    try {
      const notification = await get('/notification', {}, true);
      // this.props.set_count(notification.data.count);
    } catch {}
  }
  render() {
    const {userType} = this.state;

    return (
      <BrowserRouter>
        <Switch>
          <Main>
            {path.map((prop, key) => {
              return prop.auth ? (
                <PrivateRoute
                  permission={
                    !prop.userType || prop.userType === userType ? true : false
                  }
                  path={prop.path}
                  component={prop.component}
                  key={key}
                  isAuthenticated={this.props.isAuthenticated}
                />
              ) : (       
                <Route exact path={prop.path} component={prop.component} key={key} />
              );
            })}
          </Main>

        </Switch>
      </BrowserRouter>
    );
  }
}
const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
  };
};

export default connect(mapStateToProps, null)(App);
