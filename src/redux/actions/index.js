const add_bascket = service => ({
    type: 'ADD_TO_BASCKET',
    payload: service
});
export const add_to_bascket = service => dispatch => dispatch(add_bascket(service));


const remove_basket = service => ({
    type: 'REMOVE_BASCKET',
    payload: service,
});
export const remove_from_bascket = service => dispatch => dispatch(remove_basket(service));


const remove_all = () => ({
    type: 'REMOVE_ALL_BASCKET',
});
export const remove_all_bascket = () => dispatch => dispatch(remove_all());


const add_salon = salon => ({
    type: 'ADD_TO_SALON',
    payload: salon
});
export const add_to_salon = salon => dispatch => dispatch(add_salon(salon));

const is_Authenticated = auth => ({
    type: 'SET_AUTHENTICATE',
    payload: auth,
})
export const set_authenticated = auth => dispatch => dispatch(is_Authenticated(auth));


const add_filter = item => ({
    type: 'ADD_TO_FILTER',
    payload: item
});
export const add_to_filter = item => dispatch => dispatch(add_filter(item));