import { createStore , applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from './reducer/index';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, reducer)

const middleware = [ thunk ];
middleware.push(createLogger())


let store = createStore(persistedReducer , applyMiddleware(...middleware))
let persistor = persistStore(store)

export { persistor, store }