const addSalon = (state = [], action) => {
    switch(action.type) {
        case 'ADD_TO_SALON' : 
            return action.payload;
        default:
            return state;
    }
}

export default addSalon;