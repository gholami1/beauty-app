const itemFilter = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TO_FILTER':
            return action.payload;
        default:
            return state;
    }
}

export default itemFilter;