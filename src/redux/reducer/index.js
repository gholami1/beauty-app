import { combineReducers } from 'redux';
import  buyBasket  from './buyBasket';
import addSalon from './addSalon';
import isAuthenticated from './isAuthenticated';
import itemFilter from './itemFilter';

export default combineReducers({ 
    buyBasket,
    addSalon,
    isAuthenticated,
    itemFilter,
 });