// const check_athenticate = (state) => {
//     if(state === null){
//         const user = JSON.parse(localStorage.getItem('user'));
//         if(user){
//             return user.api_token ? 
//              true : false;
//         } else {
//             return false
//         }
//     } else
//     return state;
// }


const isAuthenticated = (state = null, action) => {
    switch (action.type) {
        case 'SET_AUTHENTICATE':
            return action.payload;
        default:
            //   return check_athenticate(state)
            return state;
    }
}

export default isAuthenticated;