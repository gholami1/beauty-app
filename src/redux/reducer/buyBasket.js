const buyBasket = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TO_BASCKET':
            if (state.filter(value => value.id === action.payload.id).length > 0)
                return state;
            else
                return [...state, action.payload]
        case 'REMOVE_BASCKET':
            const {
                id
            } = action.payload;
            return state.filter(value => value.id !== id);
        case 'REMOVE_ALL_BASCKET':
            return [];
        default:
            return state;
    }
}
export default buyBasket;