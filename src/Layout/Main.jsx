import React, { Component } from 'react';
import Menu from '../Components/Menu';

class Main extends Component {
    container = React.createRef();
    constructor(props){
        super(props);
        this.state = {
            showBox: false,
        }
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    componentWillUnmount() {
      document.removeEventListener("mousedown", this.handleClickOutside);
    }

    handleClickOutside = event => {
        if (this.container.current && !this.container.current.contains(event.target)) {
          this.setState({
            showBox: false,
          });
        }
      };

    render() {
        const { children } = this.props;
        return (
            <div className="main" ref={this.container}>
                <Menu />
                {children}
            </div>
        );
    }
}

export default Main;

// import React, { createRef, useEffect, useState } from 'react';
// import Menu from '../Components/Menu';

// const Main = (props) => {
//     container = createRef();
//     const { children } = props;
//     const [showBox , setShowBox] = useState(false);

//     useEffect(() => {
//         document.addEventListener("mousedown", handleClickOutside);

//         return () => {
//             document.removeEventListener("mousedown", handleClickOutside);
//         }
//     },[]);

  

//     const handleClickOutside = event => {
//         if (container.current && !container.current.contains(event.target)) {
//           setShowBox(false);
//         }
//       };


//     return (
//         <div className="main" ref={container}>
//             <Menu />
//             {children}
//         </div>
//     );
// }

// export default Main;